/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

USE `db-test1`;

/* Alter table in target */
ALTER TABLE `agent_info` 
	CHANGE `agent_level` `agent_level` int(11)   NULL DEFAULT 1 COMMENT '代理商级别' after `full_name` , 
	ADD COLUMN `saler_id` int(11)   NULL DEFAULT 0 COMMENT '所属销售人员ID' after `assurance_fee` , 
	CHANGE `saler_name` `saler_name` varchar(30)  COLLATE utf8_general_ci NULL COMMENT '销售员名称' after `saler_id` , 
	ADD COLUMN `province_city` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '所在省市区' after `saler_name` , 
	CHANGE `address` `address` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '详细地址' after `province_city` , 
	CHANGE `status` `status` int(11)   NULL DEFAULT 2 COMMENT '代理商状态 1-未开通 2-已开通' after `super_agent` , 
	CHANGE `open_time` `open_time` datetime   NULL DEFAULT CURRENT_TIMESTAMP DEFAULT_GENERATED COMMENT '开通时间' after `status` , 
	CHANGE `bank_name` `bank_name` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '开户行名' after `bankno` , 
	CHANGE `add_time` `add_time` datetime   NULL DEFAULT CURRENT_TIMESTAMP DEFAULT_GENERATED COMMENT '录入时间' after `bank_name` , 
	CHANGE `area_code` `area_code` varchar(6)  COLLATE utf8_general_ci NULL COMMENT '地区编码' after `biz_type` , 
	ADD COLUMN `t1_divide` int(11)   NULL COMMENT 't1分成比例' after `withdrawal_fee` , 
	CHANGE `t0_divide` `t0_divide` int(11)   NULL DEFAULT 0 COMMENT 't0分成比例' after `t1_divide` , 
	ADD COLUMN `gateway_rate_t1` decimal(12,4)   NULL DEFAULT 0.0000 COMMENT '网关T1费率' after `app_t0_rate` , 
	CHANGE `gateway_fixed` `gateway_fixed` decimal(12,2)   NULL DEFAULT 0.00 COMMENT '网关T1封顶' after `gateway_rate_t1` , 
	CHANGE `qrcode_withdrawal` `qrcode_withdrawal` decimal(12,2)   NULL DEFAULT 0.00 COMMENT '二维码提现费分成' after `gateway_rate_t0` , 
	CHANGE `biz_license_img` `biz_license_img` varchar(520)  COLLATE utf8_general_ci NULL COMMENT '营业执照' after `quick_rate_t0` , 
	ADD COLUMN `identity3_img` varchar(520)  COLLATE utf8_general_ci NULL COMMENT '身份证正面照' after `biz_license_img` , 
	CHANGE `identity1_img` `identity1_img` varchar(520)  COLLATE utf8_general_ci NULL COMMENT '身份证反面照' after `identity3_img` , 
	CHANGE `identity2_img` `identity2_img` varchar(520)  COLLATE utf8_general_ci NULL COMMENT '法人手持身份证照片' after `identity1_img` , 
	CHANGE `corporation_store_img` `corporation_store_img` varchar(520)  COLLATE utf8_general_ci NULL COMMENT '法人和线下门店合照' after `identity2_img` , 
	CHANGE `company_store_img` `company_store_img` varchar(520)  COLLATE utf8_general_ci NULL COMMENT '企业或店铺门头照片' after `corporation_store_img` , 
	CHANGE `cashier_img` `cashier_img` varchar(520)  COLLATE utf8_general_ci NULL COMMENT '前台或收银台相片' after `company_store_img` , 
	CHANGE `place_img` `place_img` varchar(520)  COLLATE utf8_general_ci NULL COMMENT '企业或店铺内部运营区域' after `cashier_img` , 
	ADD COLUMN `card1_img` varchar(520)  COLLATE utf8_general_ci NULL COMMENT '银行卡正面照片' after `place_img` , 
	ADD COLUMN `card2_img` varchar(520)  COLLATE utf8_general_ci NULL COMMENT '银行卡背面照片' after `card1_img` , 
	CHANGE `contract_img` `contract_img` varchar(520)  COLLATE utf8_general_ci NULL COMMENT '合同文件' after `card2_img` , 
	CHANGE `other_file` `other_file` varchar(520)  COLLATE utf8_general_ci NULL COMMENT '其它文件' after `contract_img` , 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' COMMENT '逻辑删除，1删除' after `other_file` , 
	ADD COLUMN `remark` varchar(2550)  COLLATE utf8_general_ci NULL COMMENT '备注' after `deleted` , 
	DROP COLUMN `saler` , 
	DROP COLUMN `province` , 
	DROP COLUMN `identity_card_img` , 
	DROP COLUMN `bank_card_img` , 
	DROP COLUMN `online_rate` , 
	DROP COLUMN `city` , 
	DROP COLUMN `bank_card_back_img` ;

/* Create table in target */
CREATE TABLE `agent_record`(
	`id` bigint(20) NOT NULL  COMMENT '主键' , 
	`create_time` datetime NOT NULL  DEFAULT CURRENT_TIMESTAMP DEFAULT_GENERATED COMMENT '添加时间' , 
	`before_value` varchar(5000) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '操作前值' , 
	`after_value` varchar(5000) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '操作后值' , 
	`operator_id` int(15) NOT NULL  COMMENT '操作人id' , 
	`ip` varbinary(25) NULL  COMMENT '操作人当前ip' , 
	`success` bit(1) NULL  COMMENT '执行结果' , 
	`request_param` varchar(5000) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '请求参数' , 
	`request_func` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '请求方法名' , 
	`remark` varchar(500) COLLATE utf8mb4_0900_ai_ci NULL  DEFAULT '操作成功！' COMMENT '备注信息' , 
	PRIMARY KEY (`id`,`operator_id`,`create_time`) 
) ENGINE=InnoDB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_0900_ai_ci';


/* Alter table in target */
ALTER TABLE `agent_roler` 
	ADD COLUMN `deleted` bit(1)   NULL COMMENT '逻辑删除' after `roler_from` ;

/* Alter table in target */
ALTER TABLE `agent_trans` 
	CHANGE `refno` `refno` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '参考号' after `type_memo` , 
	CHANGE `cost_fee` `cost_fee` decimal(12,2)   NULL COMMENT '代理商成本价' after `channel_fee` , 
	CHANGE `agent_fee` `agent_fee` decimal(12,2)   NULL DEFAULT 0.00 COMMENT '代理商手续费' after `total_agent_fee` , 
	CHANGE `pay_fee` `pay_fee` decimal(12,2)   NULL COMMENT '提现手续费' after `liquid_date` , 
	CHANGE `divide_type` `divide_type` int(11)   NULL DEFAULT 1 COMMENT '分润类别(1-银行卡 2-二维码)' after `t0_agent_fee` ;

/* Create table in target */
CREATE TABLE `auth_record`(
	`id` bigint(20) NOT NULL  auto_increment COMMENT '主键' , 
	`before_value` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '操作前值' , 
	`after_value` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '操作后值' , 
	`create_time` datetime NOT NULL  COMMENT '添加时间' , 
	`operator_id` int(15) NOT NULL  COMMENT '操作人id' , 
	`ip` varbinary(25) NULL  COMMENT '操作人当前ip' , 
	`success` bit(1) NULL  COMMENT '执行结果' , 
	`request_param` varchar(2048) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '请求参数' , 
	`request_func` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '请求方法名' , 
	PRIMARY KEY (`id`,`operator_id`) 
) ENGINE=InnoDB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_0900_ai_ci';


/* Alter table in target */
ALTER TABLE `auth_route_merch` 
	ADD COLUMN `channel_merchno` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '渠道商户号' after `channel_code` , 
	DROP COLUMN `merchno` ;

/* Create table in target */
CREATE TABLE `boss_record`(
	`id` bigint(20) NOT NULL  auto_increment COMMENT '主键' , 
	`before_value` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '操作前值' , 
	`after_value` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '操作后值' , 
	`create_time` datetime NOT NULL  COMMENT '添加时间' , 
	`operator_id` int(15) NOT NULL  COMMENT '操作人id' , 
	`ip` varbinary(25) NULL  COMMENT '操作人当前ip' , 
	`success` bit(1) NULL  COMMENT '执行结果' , 
	`request_param` varchar(2048) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '请求参数' , 
	`request_func` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '请求方法名' , 
	PRIMARY KEY (`id`,`operator_id`) 
) ENGINE=InnoDB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_0900_ai_ci';


/* Alter table in target */
ALTER TABLE `boss_user` 
	CHANGE `login_name` `login_name` varchar(30)  COLLATE utf8_general_ci NOT NULL COMMENT '登陆账号' after `branchno` , 
	CHANGE `password` `password` varchar(64)  COLLATE utf8_general_ci NOT NULL COMMENT '用户密码' after `true_name` , 
	DROP KEY `PRIMARY`, ADD PRIMARY KEY(`id`,`login_name`) ;

/* Alter table in target */
ALTER TABLE `branch_app` 
	CHANGE `branchno` `branchno` varchar(20)  COLLATE utf8_general_ci NULL COMMENT '机构编号' after `id` , 
	CHANGE `app_name` `app_name` varchar(30)  COLLATE utf8_general_ci NULL COMMENT 'APP名称' after `branchno` , 
	CHANGE `app_logo` `app_logo` varchar(255)  COLLATE utf8_general_ci NULL COMMENT 'APP-LOGO' after `app_name` , 
	CHANGE `app_version` `app_version` varchar(20)  COLLATE utf8_general_ci NULL COMMENT 'APP版本' after `app_logo` , 
	CHANGE `app_open_t0` `app_open_t0` int(11)   NULL COMMENT '开通T+0(1-未开通 2-开通)' after `app_version` , 
	CHANGE `app_open_multi` `app_open_multi` int(11)   NULL COMMENT '开通一机多费率(1-未开通 2-开通)' after `app_open_t0` , 
	CHANGE `app_auth` `app_auth` varchar(50)  COLLATE utf8_general_ci NULL COMMENT 'APP功能权限' after `app_open_multi` , 
	CHANGE `app_help_content` `app_help_content` varchar(1000)  COLLATE utf8_general_ci NULL COMMENT 'APP的帮助中心的内容' after `app_auth` , 
	CHANGE `app_myself_content` `app_myself_content` varchar(1000)  COLLATE utf8_general_ci NULL COMMENT 'APP的关于我们的内容' after `app_help_content` , 
	CHANGE `app_android_url` `app_android_url` varchar(100)  COLLATE utf8_general_ci NULL COMMENT 'Android路径' after `app_myself_content` , 
	CHANGE `app_ios_url` `app_ios_url` varchar(100)  COLLATE utf8_general_ci NULL COMMENT 'IOS路径' after `app_android_url` , 
	CHANGE `add_time` `add_time` datetime   NULL DEFAULT CURRENT_TIMESTAMP DEFAULT_GENERATED on update CURRENT_TIMESTAMP COMMENT '录入时间' after `app_rate_t1` , 
	CHANGE `open_virt` `open_virt` int(11)   NULL DEFAULT 1 COMMENT '开通虚拟账户(1-未开通 2-开通)' after `push_sec` , 
	CHANGE `open_sale` `open_sale` int(11)   NULL DEFAULT 1 COMMENT '开通分销功能(1-未开通 2-开通)' after `open_virt` , 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' COMMENT '逻辑删除1:删除' after `qq_t0_rate` , 
	ADD KEY `INDEX_NAME_BRANNO`(`branchno`,`app_name`) ;

/* Alter table in target */
ALTER TABLE `branch_biz` 
	CHANGE `open_t1` `open_t1` int(11)   NULL DEFAULT 1 COMMENT 'T+1业务开通状态(2-开通 1-关闭)' after `biz_type` , 
	CHANGE `open_t0` `open_t0` int(11)   NULL DEFAULT 1 COMMENT 'T+0业务开通状态(2-开通 1-关闭)' after `open_t1` , 
	CHANGE `add_time` `add_time` datetime   NULL DEFAULT CURRENT_TIMESTAMP DEFAULT_GENERATED on update CURRENT_TIMESTAMP COMMENT '创建时间' after `min_fee` , 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' COMMENT '逻辑删除' after `add_time` , 
	ADD KEY `INDEX_BRANCHNO_TIME`(`branchno`,`add_time`) ;

/* Alter table in target */
ALTER TABLE `branch_help` 
	CHANGE `add_time` `add_time` datetime   NULL DEFAULT CURRENT_TIMESTAMP DEFAULT_GENERATED COMMENT '创建时间' after `login_name` , 
	CHANGE `type` `type` int(11)   NULL DEFAULT 1 COMMENT '问题类型1.常见问题,2新手问题' after `add_time` , 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' COMMENT '逻辑删除' after `type` , 
	ADD KEY `INDEX_NO_KEY`(`branchno`,`key_val`) ;

/* Alter table in target */
ALTER TABLE `branch_info` 
	CHANGE `branch_name` `branch_name` varchar(50)  COLLATE utf8_general_ci NULL DEFAULT '0' COMMENT '分公司简称' after `branchno` , 
	CHANGE `province` `province` varchar(128)  COLLATE utf8_general_ci NULL COMMENT '所在省份' after `area_code` , 
	CHANGE `t0_min_amount` `t0_min_amount` decimal(12,2)   NULL COMMENT 'T0最低消费金额' after `merch_audit_judg` , 
	CHANGE `branch_logo` `branch_logo` varchar(1024)  COLLATE utf8_general_ci NULL COMMENT '分公司LOGO' after `t0_rate` , 
	CHANGE `add_time` `add_time` datetime   NULL DEFAULT CURRENT_TIMESTAMP DEFAULT_GENERATED COMMENT '录入时间' after `branch_system_name` , 
	CHANGE `t0_settle_rate` `t0_settle_rate` decimal(12,4)   NULL DEFAULT 0.0000 COMMENT 'T+0结算费率' after `add_time` , 
	CHANGE `withdrawal_fee` `withdrawal_fee` decimal(12,2)   NULL COMMENT '提现费' after `t0_settle_rate` , 
	CHANGE `t1_settle_rate` `t1_settle_rate` decimal(12,4)   NULL DEFAULT 0.0000 COMMENT 'T+1结算费率' after `withdrawal_fee` , 
	CHANGE `remind_type` `remind_type` int(11)   NULL COMMENT '提醒类型' after `t1_settle_rate` , 
	CHANGE `fee_type` `fee_type` int(11)   NULL COMMENT '费用类型' after `remind_type` , 
	CHANGE `bank_name` `bank_name` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '开户行' after `bankno` , 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' COMMENT '逻辑删除1删除' after `quick_t0_cost` , 
	ADD COLUMN `province_city` varchar(255)  COLLATE utf8_general_ci NULL COMMENT '省市区代码' after `deleted` ;

/* Create table in target */
CREATE TABLE `branch_record`(
	`id` bigint(20) NOT NULL  auto_increment COMMENT '主键' , 
	`before_value` varchar(5000) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '操作前值' , 
	`after_value` varchar(5000) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '操作后值' , 
	`create_time` datetime NOT NULL  DEFAULT CURRENT_TIMESTAMP DEFAULT_GENERATED COMMENT '添加时间' , 
	`operator_id` int(15) NOT NULL  COMMENT '操作人id' , 
	`ip` varbinary(25) NULL  COMMENT '操作人当前ip' , 
	`success` bit(1) NULL  COMMENT '执行结果' , 
	`request_param` varchar(2048) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '请求参数' , 
	`request_func` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '请求方法名' , 
	`remark` varchar(1024) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '备注' , 
	PRIMARY KEY (`id`,`operator_id`,`create_time`) 
) ENGINE=InnoDB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_0900_ai_ci';


/* Alter table in target */
ALTER TABLE `branch_roler` 
	ADD COLUMN `deleted` bit(1)   NULL COMMENT '逻辑删除' after `roler_auth` ;

/* Alter table in target */
ALTER TABLE `branch_spread` 
	CHANGE `login_name` `login_name` varchar(30)  COLLATE utf8_general_ci NULL COMMENT '创建者' after `content` , 
	ADD COLUMN `img_url` varchar(2550)  COLLATE utf8_general_ci NULL COMMENT '图片路径' after `login_name` , 
	CHANGE `add_time` `add_time` datetime   NULL DEFAULT CURRENT_TIMESTAMP DEFAULT_GENERATED COMMENT '创建时间' after `img_url` , 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' COMMENT '1删除，0正常' after `add_time` , 
	DROP COLUMN `img1` , 
	DROP COLUMN `img2` , 
	DROP COLUMN `img9` , 
	DROP COLUMN `img3` , 
	DROP COLUMN `img4` , 
	DROP COLUMN `img5` , 
	DROP COLUMN `img6` , 
	DROP COLUMN `img7` , 
	DROP COLUMN `img8` ;

/* Alter table in target */
ALTER TABLE `branch_user` 
	ADD COLUMN `deleted` bit(1)   NULL COMMENT '逻辑删除' after `error_count` ;

/* Alter table in target */
ALTER TABLE `branch_valid` 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' COMMENT '0删除' after `min_amount` ;

/* Create table in target */
CREATE TABLE `car_order_record`(
	`id` bigint(20) NOT NULL  auto_increment COMMENT '主键' , 
	`before_value` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '操作前值' , 
	`after_value` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '操作后值' , 
	`create_time` datetime NOT NULL  COMMENT '添加时间' , 
	`operator_id` int(15) NOT NULL  COMMENT '操作人id' , 
	`ip` varbinary(25) NULL  COMMENT '操作人当前ip' , 
	`success` bit(1) NULL  COMMENT '执行结果' , 
	`request_param` varchar(2048) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '请求参数' , 
	`request_func` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '请求方法名' , 
	PRIMARY KEY (`id`,`operator_id`) 
) ENGINE=InnoDB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_0900_ai_ci';


/* Alter table in target */
ALTER TABLE `change_agent_account` 
	CHANGE `orig_accountno` `orig_accountno` varchar(25)  COLLATE utf8_general_ci NULL COMMENT '原账户号账号' after `orig_account_type` , 
	CHANGE `audit_status` `audit_status` int(11)   NULL DEFAULT 1 COMMENT '1-待审核 2-审核拒绝 3-审核通过' after `audit_agent` , 
	CHANGE `audit_date` `audit_date` datetime   NULL DEFAULT CURRENT_TIMESTAMP DEFAULT_GENERATED COMMENT '审核时间' after `audit_status` , 
	CHANGE `add_time` `add_time` datetime   NULL DEFAULT CURRENT_TIMESTAMP DEFAULT_GENERATED COMMENT '变更时间' after `user_name` , 
	ADD COLUMN `deleted` bit(1)   NULL after `add_time` , 
	ADD KEY `INDEX_TIME`(`audit_date`,`add_time`) ;

/* Alter table in target */
ALTER TABLE `change_merch_account` 
	CHANGE `orig_accountno` `orig_accountno` varchar(25)  COLLATE utf8_general_ci NULL COMMENT '原账户编号' after `orig_account_type` , 
	ADD COLUMN `deleted` bit(1)   NULL COMMENT '逻辑删除' after `card3_img` ;

/* Alter table in target */
ALTER TABLE `change_merch_info` 
	CHANGE `audit_date` `audit_date` datetime   NULL COMMENT '审核时间' after `audit_status` , 
	CHANGE `add_time` `add_time` datetime   NULL DEFAULT CURRENT_TIMESTAMP DEFAULT_GENERATED COMMENT '创建时间' after `user_name` , 
	CHANGE `area_code` `area_code` varchar(40)  COLLATE utf8_general_ci NULL COMMENT '地区编号' after `add_time` , 
	CHANGE `orig_area_code` `orig_area_code` varchar(40)  COLLATE utf8_general_ci NULL COMMENT '原编号' after `area_code` , 
	ADD COLUMN `mod_time` datetime   NULL DEFAULT CURRENT_TIMESTAMP DEFAULT_GENERATED COMMENT '修改时间' after `orig_area_code` , 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' after `mod_time` ;

/* Alter table in target */
ALTER TABLE `change_merch_rate` 
	CHANGE `add_time` `add_time` datetime   NULL COMMENT '创建时间' after `user_name` , 
	ADD COLUMN `deleted` bit(1)   NULL COMMENT '逻辑删除' after `app_credit` ;

/* Alter table in target */
ALTER TABLE `channel_info` 
	ADD COLUMN `deleted` bit(1)   NULL COMMENT '逻辑删除' after `app_code` ;

/* Alter table in target */
ALTER TABLE `channel_qrcode` 
	CHANGE `channel_address` `channel_address` varchar(500)  COLLATE utf8_general_ci NULL COMMENT '渠道地址' after `channel_fee` , 
	CHANGE `return_address` `return_address` varchar(300)  COLLATE utf8_general_ci NULL COMMENT '渠道返回地址' after `channel_address` , 
	CHANGE `notify_address` `notify_address` varchar(300)  COLLATE utf8_general_ci NULL COMMENT '通知地址' after `agentno` , 
	CHANGE `pay_notify_url` `pay_notify_url` varchar(500)  COLLATE utf8_general_ci NULL COMMENT '支付通知地址' after `merch_rsa_key` , 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' COMMENT '逻辑删除' after `liquidation` , 
	ADD COLUMN `branchno` varchar(20)  COLLATE utf8_general_ci NULL COMMENT '机构编号' after `deleted` ;

/* Create table in target */
CREATE TABLE `channel_record`(
	`id` bigint(20) NOT NULL  auto_increment COMMENT '主键' , 
	`before_value` varchar(5000) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '操作前值' , 
	`after_value` varchar(5000) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '操作后值' , 
	`create_time` datetime NOT NULL  COMMENT '添加时间' , 
	`operator_id` int(15) NOT NULL  COMMENT '操作人id' , 
	`ip` varbinary(25) NULL  COMMENT '操作人当前ip' , 
	`success` bit(1) NULL  COMMENT '执行结果' , 
	`request_param` varchar(5000) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '请求参数' , 
	`request_func` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '请求方法名' , 
	`remark` varchar(1024) COLLATE utf8mb4_0900_ai_ci NULL  , 
	PRIMARY KEY (`id`,`operator_id`) 
) ENGINE=InnoDB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_0900_ai_ci';


/* Alter table in target */
ALTER TABLE `channel_route` 
	CHANGE `time_start` `time_start` timestamp   NULL COMMENT '起始时间' after `amt_end` , 
	CHANGE `time_end` `time_end` timestamp   NULL COMMENT '截止时间' after `time_start` , 
	CHANGE `settle_type` `settle_type` int(11)   NULL COMMENT '结算类型' after `area_code` ;

/* Create table in target */
CREATE TABLE `easy_record`(
	`id` bigint(20) NOT NULL  auto_increment COMMENT '主键' , 
	`before_value` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '操作前值' , 
	`after_value` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '操作后值' , 
	`create_time` datetime NOT NULL  COMMENT '添加时间' , 
	`operator_id` int(15) NOT NULL  COMMENT '操作人id' , 
	`ip` varbinary(25) NULL  COMMENT '操作人当前ip' , 
	`success` bit(1) NULL  COMMENT '执行结果' , 
	`request_param` varchar(2048) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '请求参数' , 
	`request_func` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '请求方法名' , 
	PRIMARY KEY (`id`,`operator_id`) 
) ENGINE=InnoDB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_0900_ai_ci';


/* Create table in target */
CREATE TABLE `elec_agent`(
	`id` int(11) NOT NULL  auto_increment , 
	`agentno` varchar(10) COLLATE utf8_general_ci NULL  COMMENT '代理商编号(4位地区代码+6位顺序号)' , 
	`agent_name` varchar(50) COLLATE utf8_general_ci NULL  COMMENT '代理商简称' , 
	`full_name` varchar(60) COLLATE utf8_general_ci NULL  COMMENT '代理商全称' , 
	`agent_level` int(11) NULL  DEFAULT 0 COMMENT '代理商级别' , 
	`legal_name` varchar(15) COLLATE utf8_general_ci NULL  COMMENT '法人姓名' , 
	`identity_card` varchar(18) COLLATE utf8_general_ci NULL  COMMENT '法人身份证号' , 
	`link_man` varchar(15) COLLATE utf8_general_ci NULL  COMMENT '联系人' , 
	`email` varchar(50) COLLATE utf8_general_ci NULL  COMMENT '电子邮箱' , 
	`telephone` varchar(20) COLLATE utf8_general_ci NULL  COMMENT '固定电话' , 
	`mobile` varchar(11) COLLATE utf8_general_ci NULL  COMMENT '手机号码' , 
	`member_fee` decimal(10,2) NULL  COMMENT '加盟费' , 
	`assurance_fee` decimal(10,2) NULL  COMMENT '保证金' , 
	`saler` int(11) NULL  DEFAULT 0 COMMENT '所属销售人员ID' , 
	`saler_name` varchar(30) COLLATE utf8_general_ci NULL  COMMENT '销售员名称' , 
	`province` varchar(20) COLLATE utf8_general_ci NULL  COMMENT '所在省份' , 
	`city` varchar(30) COLLATE utf8_general_ci NULL  COMMENT '所在城市' , 
	`address` varchar(50) COLLATE utf8_general_ci NULL  COMMENT '详细地址' , 
	`super_agent` varchar(20) COLLATE utf8_general_ci NULL  COMMENT '上级代理商编号(运营平台为1000000000)' , 
	`status` int(11) NULL  DEFAULT 1 COMMENT '代理商状态 1-未开通 2-已开通' , 
	`open_time` varchar(19) COLLATE utf8_general_ci NULL  COMMENT '开通时间' , 
	`audit_status` int(11) NULL  DEFAULT 1 COMMENT '审核状态 1-待审核 2-审核拒绝 3-审核通过' , 
	`audit_agentno` varchar(20) COLLATE utf8_general_ci NULL  COMMENT '审核对象(运营平台为1000000000)' , 
	`divide` int(11) NULL  DEFAULT 0 COMMENT '与平台的分成比例(范围:0-100)' , 
	`accountno` varchar(25) COLLATE utf8_general_ci NULL  COMMENT '结算账号' , 
	`account_type` int(11) NULL  COMMENT '帐户类型 1-对私 2-对公' , 
	`account_name` varchar(35) COLLATE utf8_general_ci NULL  COMMENT '帐户户名' , 
	`bankno` varchar(12) COLLATE utf8_general_ci NULL  COMMENT '开户行行号' , 
	`bank_name` varchar(50) COLLATE utf8_general_ci NULL  , 
	`identity_card_img` varchar(100) COLLATE utf8_general_ci NULL  COMMENT '身份证照片路径' , 
	`contract_img` varchar(100) COLLATE utf8_general_ci NULL  COMMENT '合同文件路径' , 
	`other_file` varchar(100) COLLATE utf8_general_ci NULL  COMMENT '其它文件路径' , 
	`add_time` varchar(19) COLLATE utf8_general_ci NULL  COMMENT '录入时间' , 
	`user_id` int(11) NULL  , 
	`agent_type` int(11) NULL  COMMENT '代理商类型' , 
	`branchno` varchar(20) COLLATE utf8_general_ci NULL  COMMENT '分公司编码' , 
	`agentno_first` varchar(20) COLLATE utf8_general_ci NULL  COMMENT '一级代理商编号' , 
	`biz_type` int(11) NULL  COMMENT '开通的业务类型' , 
	`bank_card_img` varchar(100) COLLATE utf8_general_ci NULL  COMMENT '银行卡照片' , 
	`area_code` varchar(6) COLLATE utf8_general_ci NULL  COMMENT '地区编码' , 
	`withdrawal_fee` decimal(12,2) NULL  DEFAULT 0.00 COMMENT 'pos提现费率分成' , 
	`t0_divide` int(11) NULL  DEFAULT 0 COMMENT 't0分成比例' , 
	`pos_debit_rate` decimal(12,4) NULL  DEFAULT 0.0000 COMMENT '借记卡POS费率' , 
	`pos_debit_fixed` decimal(12,2) NULL  DEFAULT 0.00 COMMENT '借记卡POS封顶' , 
	`pos_credit_rate` decimal(12,4) NULL  DEFAULT 0.0000 COMMENT '信用卡POS费率' , 
	`app_debit_rate` decimal(12,4) NULL  DEFAULT 0.0000 COMMENT '借记卡APP费率' , 
	`app_debit_fixed` decimal(12,2) NULL  DEFAULT 0.00 COMMENT '借记卡APP封顶' , 
	`app_credit_rate` decimal(12,4) NULL  DEFAULT 0.0000 COMMENT '信用卡APP费率' , 
	`pos_t0_rate` decimal(12,4) NULL  DEFAULT 0.0000 COMMENT 'POST+0费率增量' , 
	`app_t0_rate` decimal(12,4) NULL  DEFAULT 0.0000 COMMENT 'APPT+0费率增量' , 
	`online_rate` decimal(12,4) NULL  DEFAULT 0.0000 COMMENT '线上费率' , 
	`gateway_fixed` decimal(12,2) NULL  DEFAULT 0.00 COMMENT '网关T1封顶' , 
	`gateway_rate_t0` decimal(12,4) NULL  DEFAULT 0.0000 COMMENT '网关T0费率' , 
	`qrcode_withdrawal` decimal(12,2) NULL  DEFAULT 0.00 COMMENT '提现费分成' , 
	`quick_rate_t1` decimal(12,4) NULL  COMMENT '快捷T+1成本' , 
	`quick_rate_t0` decimal(12,4) NULL  COMMENT '快捷T+0成本' , 
	`biz_license_img` varchar(100) COLLATE utf8_general_ci NULL  COMMENT '结算卡号' , 
	`identity1_img` varchar(100) COLLATE utf8_general_ci NULL  COMMENT '结算卡号' , 
	`identity2_img` varchar(100) COLLATE utf8_general_ci NULL  COMMENT '结算卡号' , 
	`corporation_store_img` varchar(100) COLLATE utf8_general_ci NULL  COMMENT '结算卡号' , 
	`company_store_img` varchar(100) COLLATE utf8_general_ci NULL  COMMENT '结算卡号' , 
	`cashier_img` varchar(100) COLLATE utf8_general_ci NULL  COMMENT '结算卡号' , 
	`place_img` varchar(100) COLLATE utf8_general_ci NULL  , 
	`bank_card_back_img` varchar(100) COLLATE utf8_general_ci NULL  COMMENT '银行卡背面' , 
	`deleted` bit(1) NULL  COMMENT '逻辑删除' , 
	PRIMARY KEY (`id`) , 
	UNIQUE KEY `agentno`(`agentno`) , 
	UNIQUE KEY `agent_name`(`agent_name`) , 
	UNIQUE KEY `full_name`(`full_name`) , 
	KEY `audit_status`(`audit_status`) , 
	KEY `id`(`id`) , 
	KEY `branchno`(`branchno`) 
) ENGINE=InnoDB DEFAULT CHARSET='utf8' COLLATE='utf8_general_ci';


/* Create table in target */
CREATE TABLE `elec_auth`(
	`id` bigint(20) NOT NULL  auto_increment , 
	`auth_name` varchar(125) COLLATE utf8mb4_0900_ai_ci NOT NULL  COMMENT '权限名称' , 
	`auth_path` varchar(125) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '权限路径' , 
	`auth_super_id` bigint(20) NULL  COMMENT '上级id' , 
	`auth_des` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '描述' , 
	`auth_date` datetime NOT NULL  DEFAULT CURRENT_TIMESTAMP DEFAULT_GENERATED COMMENT '创建日期' , 
	`auth_type` int(1) NULL  DEFAULT 1 COMMENT '1:web前端, 2:web后端' , 
	`auth_status` int(1) NULL  DEFAULT 1 COMMENT '0:停用,1:正常,2:锁定' , 
	`auth_op` int(6) NULL  DEFAULT 888888 COMMENT '操作人编号' , 
	`deleted` bit(1) NULL  DEFAULT b'0' , 
	`is_leaf` bit(1) NULL  DEFAULT b'0' COMMENT '是否为最后叶子节点1:true, 0:false' , 
	PRIMARY KEY (`id`) , 
	KEY `INDEX_AUTH_SUPPER_ID`(`auth_super_id`) 
) ENGINE=InnoDB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_0900_ai_ci';


/* Create table in target */
CREATE TABLE `elec_auth_group`(
	`id` bigint(20) NOT NULL  auto_increment , 
	`auth_id` bigint(20) NOT NULL  COMMENT '权限集合(多个使用英文,分割)' , 
	PRIMARY KEY (`id`,`auth_id`) 
) ENGINE=InnoDB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_0900_ai_ci';


/* Create table in target */
CREATE TABLE `elec_branch`(
	`id` int(11) NOT NULL  auto_increment , 
	`branchno` varchar(20) COLLATE utf8_general_ci NOT NULL  COMMENT '机构代码' , 
	`branch_name` varchar(50) COLLATE utf8_general_ci NULL  COMMENT '机构简称' , 
	`branch_full` varchar(50) COLLATE utf8_general_ci NULL  COMMENT '机构全称' , 
	`link_man` varchar(15) COLLATE utf8_general_ci NULL  COMMENT '联系人' , 
	`email` varchar(50) COLLATE utf8_general_ci NULL  COMMENT '电子邮箱' , 
	`telephone` varchar(20) COLLATE utf8_general_ci NULL  COMMENT '固定电话' , 
	`mobile` varchar(11) COLLATE utf8_general_ci NULL  COMMENT '手机号码' , 
	`area_code` varchar(10) COLLATE utf8_general_ci NULL  COMMENT '地区代码' , 
	`province` varchar(20) COLLATE utf8_general_ci NULL  COMMENT '所在省份' , 
	`city` varchar(30) COLLATE utf8_general_ci NULL  COMMENT '所在城市' , 
	`address` varchar(50) COLLATE utf8_general_ci NULL  COMMENT '详细地址' , 
	`biz_code` varchar(3) COLLATE utf8_general_ci NULL  COMMENT '业务编码(即商户和代理商编码前3位)' , 
	`agent_audit_mode` int(11) NULL  COMMENT '代理商审核模式1-无需审核 2-仅分公司审核' , 
	`merch_audit_mode` int(11) NULL  COMMENT '商户审核模式1-无需审核 2-分公司复审 3-分公司初审+复审' , 
	`merch_default_mcc` varchar(4) COLLATE utf8_general_ci NULL  COMMENT '商户默认的MCC码' , 
	`open_multi` int(11) NULL  COMMENT '开通一机多费率(1-未开通 2-已开通)' , 
	`open_t0` int(11) NULL  COMMENT '开通T+0(1-未开通 2-已开通)' , 
	`merch_audit_judg` int(11) NULL  COMMENT '商户是否需要总公司终审（1：不需要 2：需要）' , 
	`t0_min_amount` decimal(12,2) NULL  , 
	`t0_rate` decimal(12,4) NULL  COMMENT 'T+0的费率' , 
	`branch_logo` varchar(50) COLLATE utf8_general_ci NULL  COMMENT '分公司LOGO' , 
	`branch_system_name` varchar(50) COLLATE utf8_general_ci NULL  COMMENT '分公司系统名称' , 
	`add_time` varchar(19) COLLATE utf8_general_ci NULL  COMMENT '录入时间' , 
	`t0_settle_rate` decimal(12,4) NULL  , 
	`withdrawal_fee` decimal(12,2) NULL  , 
	`t1_settle_rate` decimal(12,4) NULL  , 
	`remind_type` int(11) NULL  COMMENT 'ÌáÐÑ·½Ê½' , 
	`fee_type` int(11) NULL  COMMENT '·ÖÈóÄ£Ê½' , 
	`saler_id` int(11) NULL  , 
	`accountno` varchar(25) COLLATE utf8_general_ci NULL  COMMENT '结算账号' , 
	`account_name` varchar(35) COLLATE utf8_general_ci NULL  COMMENT '结算户名' , 
	`bankno` varchar(12) COLLATE utf8_general_ci NULL  COMMENT '开户行行号' , 
	`bank_name` varchar(50) COLLATE utf8_general_ci NULL  , 
	`t1_min_amount` decimal(12,2) NULL  DEFAULT 0.00 COMMENT 'T1最低消费金额' , 
	`merch_min_fee` decimal(12,2) NULL  DEFAULT 0.00 COMMENT '商户最低手续费' , 
	`wx_rate` decimal(12,4) NULL  DEFAULT 0.0000 COMMENT '微信费率' , 
	`zfb_rate` decimal(12,4) NULL  DEFAULT 0.0000 COMMENT '支付宝费率' , 
	`bd_rate` decimal(12,4) NULL  DEFAULT 0.0000 COMMENT '百度钱包费率' , 
	`qq_rate` decimal(12,4) NULL  DEFAULT 0.0000 COMMENT 'QQ钱包费率' , 
	`jd_rate` decimal(12,4) NULL  DEFAULT 0.0000 COMMENT '京东费率' , 
	`gateway_rate_t1` decimal(12,4) NULL  DEFAULT 0.0000 COMMENT '网关T1费率' , 
	`gateway_rate_t0` decimal(12,4) NULL  DEFAULT 0.0000 COMMENT '网关T0费率' , 
	`app_id` varchar(30) COLLATE utf8_general_ci NULL  COMMENT '公众号ID' , 
	`qrcode_t1_min` decimal(12,2) NULL  DEFAULT 0.00 COMMENT 'T+1最低交易金额' , 
	`qrcode_t0_min` decimal(12,2) NULL  DEFAULT 0.00 COMMENT 'T+0最低交易金额' , 
	`qrcode_withdrawal_cost` decimal(12,2) NULL  DEFAULT 0.00 COMMENT '提现费成本' , 
	`qrcode_withdrawal_merch` decimal(12,2) NULL  DEFAULT 0.00 COMMENT '商户提现费' , 
	`qrcode_min_fee` decimal(12,2) NULL  DEFAULT 0.00 COMMENT '商户最低手续费' , 
	`open_qrcode_t1` int(11) NULL  DEFAULT 2 COMMENT '二维码T+1业务(1-关闭 2-开通)' , 
	`open_qrcode_t0` int(11) NULL  DEFAULT 2 COMMENT '二维码T+0业务(1-关闭 2-开通)' , 
	`login_type` int(11) NULL  DEFAULT 1 COMMENT '1-手机登录 2-邮箱登录' , 
	`quick_t1_cost` decimal(12,4) NULL  DEFAULT 0.0000 COMMENT '快捷T+1成本' , 
	`quick_t0_cost` decimal(12,4) NULL  DEFAULT 0.0000 COMMENT '快捷T+0成本' , 
	`deleted` bit(1) NULL  COMMENT '逻辑删除' , 
	PRIMARY KEY (`id`,`branchno`) , 
	KEY `branchno`(`branchno`) 
) ENGINE=InnoDB DEFAULT CHARSET='utf8' COLLATE='utf8_general_ci';


/* Create table in target */
CREATE TABLE `elec_user`(
	`id` int(11) NOT NULL  auto_increment , 
	`user_type` int(11) NOT NULL  DEFAULT 0 COMMENT '用户类型1.系统人员，2.机构人员,3.代理商，4.商户人员，5.虚拟账户' , 
	`user_role_no` varchar(6) COLLATE utf8_general_ci NOT NULL  COMMENT '角色编号' , 
	`user_parent_no` varchar(20) COLLATE utf8_general_ci NULL  COMMENT '所属上级编号(当用户类型为3,4时必填,由前端限制)' , 
	`user_login_name` varchar(30) COLLATE utf8_general_ci NOT NULL  COMMENT '登陆账号' , 
	`user_true_name` varchar(30) COLLATE utf8_general_ci NULL  COMMENT '真实姓名' , 
	`user_password` varchar(128) COLLATE utf8_general_ci NOT NULL  COMMENT '用户密码' , 
	`user_phone` varchar(15) COLLATE utf8_general_ci NULL  COMMENT '固定电话' , 
	`user_mobile` varchar(15) COLLATE utf8_general_ci NULL  COMMENT '手机号码' , 
	`user_email` varchar(30) COLLATE utf8_general_ci NULL  COMMENT '电子邮箱' , 
	`user_address` varchar(255) COLLATE utf8_general_ci NULL  COMMENT '地址' , 
	`user_status` int(2) NULL  DEFAULT 1 COMMENT '用户状态 1-正常 2-冻结 3-锁定 4-待启用' , 
	`user_last_login` datetime NULL  DEFAULT CURRENT_TIMESTAMP DEFAULT_GENERATED on update CURRENT_TIMESTAMP COMMENT '上次登陆时间' , 
	`user_login_error_date` datetime NULL  DEFAULT CURRENT_TIMESTAMP DEFAULT_GENERATED on update CURRENT_TIMESTAMP COMMENT '登陆错误日期' , 
	`user_error_count` int(11) NULL  DEFAULT 0 COMMENT '当天登陆错误次数' , 
	`user_reg_ip` varchar(30) COLLATE utf8_general_ci NULL  COMMENT '注册的IP' , 
	`user_reg_time` datetime NULL  DEFAULT CURRENT_TIMESTAMP DEFAULT_GENERATED COMMENT '注册的时间' , 
	`user_reg_remark` varchar(255) COLLATE utf8_general_ci NULL  COMMENT '注册的备注' , 
	`user_avatar` varchar(255) COLLATE utf8_general_ci NULL  COMMENT '头像地址' , 
	PRIMARY KEY (`id`,`user_role_no`) , 
	KEY `INDEX_USER_NO`(`user_role_no`) , 
	KEY `id`(`id`) 
) ENGINE=InnoDB DEFAULT CHARSET='utf8' COLLATE='utf8_general_ci';


/* Create table in target */
CREATE TABLE `elec_iptable`(
	`id` bigint(20) NOT NULL  auto_increment COMMENT '主键' , 
	`ip` varchar(16) COLLATE utf8mb4_0900_ai_ci NOT NULL  COMMENT 'ip地址' , 
	`status` int(2) NULL  COMMENT '0.封禁，1.正常' , 
	`create_date` datetime NULL  DEFAULT CURRENT_TIMESTAMP DEFAULT_GENERATED COMMENT '创建日期' , 
	`op` varchar(10) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '操作人id' , 
	PRIMARY KEY (`id`,`ip`) 
) ENGINE=InnoDB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_0900_ai_ci';


/* Create table in target */
CREATE TABLE `elec_menu`(
	`id` bigint(20) NOT NULL  auto_increment COMMENT '主键' , 
	`menu_name` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '菜单名' , 
	`menu_path` varchar(255) COLLATE utf8mb4_0900_ai_ci NOT NULL  COMMENT '菜单路径' , 
	`menu_date` timestamp NOT NULL  DEFAULT CURRENT_TIMESTAMP DEFAULT_GENERATED on update CURRENT_TIMESTAMP COMMENT '创建时间' , 
	`menu_order` int(3) NULL  COMMENT '排序标记' , 
	`menu_icon` varchar(25) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '菜单icon' , 
	`menu_op` int(8) NOT NULL  DEFAULT 888888 COMMENT '创建人' , 
	`menu_des` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '描述' , 
	`menu_id` int(20) NULL  DEFAULT 0 COMMENT '上级id' , 
	`menu_level` int(2) NOT NULL  DEFAULT 1 COMMENT '菜单深度' , 
	`menu_i1n8` varchar(50) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '国际化' , 
	`deleted` int(2) NULL  DEFAULT 0 , 
	PRIMARY KEY (`id`) 
) ENGINE=InnoDB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_0900_ai_ci';


/* Create table in target */
CREATE TABLE `elec_merch`(
	`id` int(11) NOT NULL  auto_increment , 
	`merchno` varchar(15) COLLATE utf8_general_ci NULL  COMMENT '商户编号' , 
	`merch_name` varchar(50) COLLATE utf8_general_ci NULL  COMMENT '商户简称' , 
	`full_name` varchar(50) COLLATE utf8_general_ci NULL  COMMENT '商户全称' , 
	`merch_type` int(11) NULL  DEFAULT 1 COMMENT '商户类别(1-个人 2-企业)' , 
	`organ_no` varchar(20) COLLATE utf8_general_ci NULL  COMMENT '客户编号' , 
	`biz_license` varchar(30) COLLATE utf8_general_ci NULL  COMMENT '营业执照' , 
	`legal_name` varchar(15) COLLATE utf8_general_ci NULL  COMMENT '法人姓名' , 
	`identity_card` varchar(18) COLLATE utf8_general_ci NULL  COMMENT '法人身份证号' , 
	`link_man` varchar(15) COLLATE utf8_general_ci NULL  COMMENT '联系人' , 
	`email` varchar(50) COLLATE utf8_general_ci NULL  COMMENT '电子邮箱' , 
	`telephone` varchar(20) COLLATE utf8_general_ci NULL  COMMENT '固定电话' , 
	`mobile` varchar(11) COLLATE utf8_general_ci NULL  COMMENT '手机号码' , 
	`saler` int(11) NULL  DEFAULT 0 COMMENT '所属销售人员ID' , 
	`saler_name` varchar(30) COLLATE utf8_general_ci NULL  COMMENT '销售员名称' , 
	`mcc` varchar(4) COLLATE utf8_general_ci NULL  COMMENT 'MCC码' , 
	`industry_type` int(11) NULL  COMMENT '行业类型' , 
	`area_code` varchar(10) COLLATE utf8_general_ci NULL  DEFAULT '000000' COMMENT '地区代码' , 
	`province` varchar(20) COLLATE utf8_general_ci NULL  COMMENT '所在省份' , 
	`city` varchar(30) COLLATE utf8_general_ci NULL  COMMENT '所在城市' , 
	`address` varchar(100) COLLATE utf8_general_ci NULL  COMMENT '详细地址' , 
	`super_agent` varchar(20) COLLATE utf8_general_ci NULL  COMMENT '上级代理商编号(运营平台为1000000000)' , 
	`status` int(11) NULL  DEFAULT 1 COMMENT '商户状态 1-未开通 2-已开通' , 
	`open_time` varchar(19) COLLATE utf8_general_ci NULL  COMMENT '开通时间' , 
	`audit_status` int(11) NULL  DEFAULT 1 COMMENT '审核状态 1-待审核 2-审核拒绝 3-审核通过' , 
	`judg_status` int(11) NULL  DEFAULT 1 COMMENT '终审状态 1-待审核 2-审核拒绝 3-审核通过 4-总公司待审核' , 
	`audit_agentno` varchar(20) COLLATE utf8_general_ci NULL  COMMENT '审核对象(运营平台为1000000000)' , 
	`rate_mode` int(11) NULL  DEFAULT 1 COMMENT '费率模式 1-固定费率 2-一机多费率' , 
	`debit_rate` decimal(5,4) NULL  COMMENT '借记卡费率' , 
	`debit_fixed` decimal(10,2) NULL  COMMENT '借记卡封顶金额' , 
	`credit_rate` decimal(5,4) NULL  COMMENT '贷记卡费率' , 
	`credit_fixed` decimal(10,2) NULL  COMMENT '贷记卡封顶金额' , 
	`stripe_card_fixed` decimal(12,2) NULL  DEFAULT 30000.00 , 
	`chip_card_fixed` decimal(12,2) NULL  DEFAULT 30000.00 , 
	`agent_fee_id` int(11) NULL  DEFAULT 0 COMMENT '代理商费率编号' , 
	`settle_type` int(11) NULL  DEFAULT 2 COMMENT '结算类型(多选:1-T+0结算 2-T+1结算 4-T+5结算 )' , 
	`accountno` varchar(25) COLLATE utf8_general_ci NULL  COMMENT '结算账号' , 
	`account_type` int(11) NULL  COMMENT '帐户类型 1-对私 2-对公' , 
	`account_name` varchar(35) COLLATE utf8_general_ci NULL  COMMENT '帐户户名' , 
	`bankno` varchar(12) COLLATE utf8_general_ci NULL  COMMENT '开户行行号' , 
	`bank_name` varchar(50) COLLATE utf8_general_ci NULL  , 
	`amt_limit` decimal(10,2) NULL  COMMENT '单笔限额(单位:元)' , 
	`day_limit` int(11) NULL  DEFAULT 100 COMMENT '单日限额(单位:万元)' , 
	`month_limit` int(11) NULL  DEFAULT 1000 COMMENT '单月限额(单位:万元)' , 
	`trans_ctrl` int(11) NULL  COMMENT '开通功能(多选1-消费 2-退货 4-余额查询 8-预授权)' , 
	`biz_license_img` varchar(100) COLLATE utf8_general_ci NULL  COMMENT '营业执照' , 
	`account_img` varchar(100) COLLATE utf8_general_ci NULL  COMMENT '开户许可证照' , 
	`identity1_img` varchar(100) COLLATE utf8_general_ci NULL  COMMENT '手持身份证照' , 
	`identity2_img` varchar(100) COLLATE utf8_general_ci NULL  COMMENT '身份证正面照' , 
	`identity3_img` varchar(100) COLLATE utf8_general_ci NULL  COMMENT '身份证反面照' , 
	`card1_img` varchar(100) COLLATE utf8_general_ci NULL  COMMENT '银行卡正面' , 
	`card2_img` varchar(100) COLLATE utf8_general_ci NULL  COMMENT '银行卡反面' , 
	`credit1_img` varchar(100) COLLATE utf8_general_ci NULL  COMMENT '信用卡正面' , 
	`credit2_img` varchar(100) COLLATE utf8_general_ci NULL  COMMENT '手持信用卡' , 
	`cashier_img` varchar(100) COLLATE utf8_general_ci NULL  COMMENT '收银台照' , 
	`place_img` varchar(100) COLLATE utf8_general_ci NULL  COMMENT '经营场所照' , 
	`head_img` varchar(100) COLLATE utf8_general_ci NULL  COMMENT '门头照' , 
	`contract_img` varchar(100) COLLATE utf8_general_ci NULL  COMMENT '合同' , 
	`other_file` varchar(100) COLLATE utf8_general_ci NULL  COMMENT '其它文件路径' , 
	`add_time` varchar(19) COLLATE utf8_general_ci NULL  COMMENT '录入时间' , 
	`first_agentno` varchar(20) COLLATE utf8_general_ci NULL  COMMENT '一级代理商编号' , 
	`branchno` varchar(20) COLLATE utf8_general_ci NULL  COMMENT '分公司编码' , 
	`saler_id` int(11) NULL  COMMENT '业务员ID' , 
	`biz_type` int(11) NULL  COMMENT '业务类型' , 
	`trad_rate` decimal(12,4) NULL  , 
	`t0_rate` decimal(12,4) NULL  DEFAULT 0.0070 COMMENT 'T+0费率' , 
	`merch_level` int(11) NULL  COMMENT '商户级别' , 
	`customerno` varchar(30) COLLATE utf8_general_ci NULL  , 
	`silence_merch` varchar(5) COLLATE utf8_general_ci NULL  DEFAULT 'N' COMMENT '沉默商户' , 
	`t0_fixed` decimal(12,2) NULL  DEFAULT 0.00 , 
	`app_rate` decimal(12,4) NULL  DEFAULT 0.0000 COMMENT 'app费率' , 
	`app_fixed` decimal(12,2) NULL  DEFAULT 0.00 COMMENT 'app封顶' , 
	`manu_rate` decimal(12,4) NULL  DEFAULT 0.0000 COMMENT '线上费率' , 
	`manu_fixed` decimal(12,2) NULL  DEFAULT 0.00 COMMENT '线上封顶' , 
	`wx_rate` decimal(12,4) NULL  DEFAULT 0.0000 COMMENT '微信费率' , 
	`zfb_rate` decimal(12,4) NULL  DEFAULT 0.0000 COMMENT '支付宝费率' , 
	`bd_rate` decimal(12,4) NULL  DEFAULT 0.0000 COMMENT '百度钱包费率' , 
	`qq_rate` decimal(12,4) NULL  DEFAULT 0.0000 COMMENT 'QQ钱包费率' , 
	`jd_rate` decimal(12,4) NULL  DEFAULT 0.0000 COMMENT '京东费率' , 
	`merch_key` varchar(32) COLLATE utf8_general_ci NULL  COMMENT '????' , 
	`online_url_notify` varchar(100) COLLATE utf8_general_ci NULL  COMMENT '??????' , 
	`online_url_return` varchar(100) COLLATE utf8_general_ci NULL  COMMENT '??????' , 
	`app_credit` decimal(12,4) NULL  DEFAULT 0.0000 COMMENT 'APP贷记卡费率' , 
	`app_t0_rate` decimal(12,4) NULL  DEFAULT 0.0000 COMMENT 'APPT+0费率增量' , 
	`gateway_rate_t0` decimal(12,4) NULL  DEFAULT 0.0000 COMMENT '网关T0费率' , 
	`sale_level` int(11) NULL  DEFAULT 5 COMMENT '分销商级别(1-BOSS 2-高级 3-中级 4-低级 5-不属于分销商)' , 
	`quick_rate_t1` decimal(12,4) NULL  DEFAULT 0.0063 COMMENT '快捷T+1费率' , 
	`quick_rate_t0` decimal(12,4) NULL  DEFAULT 0.0063 COMMENT '快捷T+0费率' , 
	`zk_rate` int(11) NULL  DEFAULT 0 COMMENT '折扣比例' , 
	`zk_time_start` varchar(8) COLLATE utf8_general_ci NULL  DEFAULT '00:00:00' COMMENT '折扣起始时间' , 
	`zk_time_end` varchar(8) COLLATE utf8_general_ci NULL  DEFAULT '23:59:59' COMMENT '折扣截止时间' , 
	`zk_amount` decimal(12,2) NULL  DEFAULT 0.00 COMMENT '折扣金额' , 
	`corporation_store_img` varchar(100) COLLATE utf8_general_ci NULL  , 
	`deleted` bit(1) NULL  COMMENT '逻辑删除' , 
	PRIMARY KEY (`id`) , 
	KEY `merch_index_1`(`merchno`) , 
	KEY `id`(`id`) , 
	KEY `audit_status`(`audit_status`) , 
	KEY `status`(`status`) , 
	KEY `silence_merch`(`silence_merch`) , 
	KEY `judg_status`(`judg_status`) , 
	KEY `merch_type`(`merch_type`) 
) ENGINE=InnoDB DEFAULT CHARSET='utf8' COLLATE='utf8_general_ci';


/* Create table in target */
CREATE TABLE `elec_role`(
	`id` bigint(20) NOT NULL  auto_increment , 
	`role_name` varchar(25) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '角色名' , 
	`role_no` varchar(6) COLLATE utf8mb4_0900_ai_ci NOT NULL  COMMENT '角色编号' , 
	`role_memo` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '角色说明' , 
	`role_type` int(4) NOT NULL  COMMENT '角色类型0.系统，1.机构，2.代理商，3.商户,4.账户' , 
	PRIMARY KEY (`id`,`role_no`) , 
	KEY `INDEX_ROLE_NO`(`role_no`) 
) ENGINE=InnoDB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_0900_ai_ci';


/* Create table in target */
CREATE TABLE `elec_role_auth`(
	`id` bigint(20) NOT NULL  auto_increment , 
	`role_id` bigint(20) NOT NULL  DEFAULT 3 COMMENT '角色表主键' , 
	`auth_id` bigint(20) NOT NULL  COMMENT '权限表主键' , 
	PRIMARY KEY (`id`) , 
	KEY `INDEX_NO`(`role_id`,`auth_id`) 
) ENGINE=InnoDB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_0900_ai_ci';


/* Create table in target */
CREATE TABLE `elec_user_agent_info`(
	`id` bigint(20) NOT NULL  auto_increment COMMENT '主键' , 
	`account_id` int(11) NOT NULL  COMMENT '账号表主键' , 
	`agent_id` int(11) NOT NULL  COMMENT '代理商表主键' , 
	PRIMARY KEY (`id`,`account_id`,`agent_id`) , 
	KEY `AGENT_ID`(`agent_id`) , 
	KEY `ACCOUNT_ID`(`account_id`) , 
	CONSTRAINT `ACCOUNT_ID` 
	FOREIGN KEY (`account_id`) REFERENCES `elec_user` (`id`) ,
	CONSTRAINT `AGENT_ID` 
	FOREIGN KEY (`agent_id`) REFERENCES `agent_info` (`id`) 
) ENGINE=InnoDB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_0900_ai_ci';


/* Create table in target */
CREATE TABLE `elec_user_branch_info`(
	`id` bigint(20) NOT NULL  auto_increment COMMENT '主键' , 
	`account_id` int(11) NOT NULL  COMMENT '账号表主键' , 
	`branch_id` int(11) NOT NULL  COMMENT '机构表主键' , 
	PRIMARY KEY (`id`) , 
	KEY `BRANCH_ID`(`branch_id`) , 
	KEY `BRANCH_ACCOUNT_ID`(`account_id`) , 
	CONSTRAINT `BRANCH_ACCOUNT_ID` 
	FOREIGN KEY (`account_id`) REFERENCES `elec_user` (`id`) ,
	CONSTRAINT `BRANCH_ID` 
	FOREIGN KEY (`branch_id`) REFERENCES `branch_info` (`id`) 
) ENGINE=InnoDB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_0900_ai_ci';


/* Create table in target */
CREATE TABLE `elec_user_merch_info`(
	`id` bigint(20) NOT NULL  auto_increment COMMENT '主键' , 
	`account_id` int(11) NOT NULL  COMMENT '账号表主键' , 
	`merch_id` int(11) NOT NULL  COMMENT '商户表主键' , 
	PRIMARY KEY (`id`) , 
	KEY `MERCH_ID`(`merch_id`) , 
	KEY `BRANCH_ACCOUNT_ID`(`account_id`) , 
	CONSTRAINT `MERCH_ID_FORKEY` 
	FOREIGN KEY (`merch_id`) REFERENCES `merch_info` (`id`) , 
	CONSTRAINT `MERCH_ID_FORKEY2` 
	FOREIGN KEY (`account_id`) REFERENCES `elec_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_0900_ai_ci';


/* Create table in target */
CREATE TABLE `finance_record`(
	`id` bigint(20) NOT NULL  auto_increment COMMENT '主键' , 
	`before_value` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '操作前值' , 
	`after_value` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '操作后值' , 
	`create_time` datetime NOT NULL  COMMENT '添加时间' , 
	`operator_id` int(15) NOT NULL  COMMENT '操作人id' , 
	`ip` varbinary(25) NULL  COMMENT '操作人当前ip' , 
	`success` bit(1) NULL  COMMENT '执行结果' , 
	`request_param` varchar(2048) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '请求参数' , 
	`request_func` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '请求方法名' , 
	PRIMARY KEY (`id`,`operator_id`) 
) ENGINE=InnoDB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_0900_ai_ci';


/* Alter table in target */
ALTER TABLE `merch_info` 
	CHANGE `merchno` `merchno` varchar(50)  COLLATE utf8_general_ci NOT NULL COMMENT '商户编号' after `id` , 
	CHANGE `merch_type` `merch_type` int(11)   NULL DEFAULT 1 COMMENT '商户类别(1-MPOS，2-传统POS，4-二维码，8-网关，16-快捷)' after `full_name` , 
	ADD COLUMN `customer_no` varchar(20)  COLLATE utf8_general_ci NULL COMMENT '客户编号' after `merch_type` , 
	CHANGE `legal_name` `legal_name` varchar(15)  COLLATE utf8_general_ci NULL COMMENT '法人姓名' after `customer_no` , 
	CHANGE `industry_type` `industry_type` int(11)   NULL COMMENT '行业类型1民生类,2一般类,4餐娱类,8批发类,16房汽类' after `mcc` , 
	ADD COLUMN `province_city` varchar(40)  COLLATE utf8_general_ci NULL COMMENT '省市区代码' after `industry_type` , 
	CHANGE `address` `address` varchar(100)  COLLATE utf8_general_ci NULL COMMENT '详细地址' after `province_city` , 
	CHANGE `status` `status` int(11)   NULL DEFAULT 0 COMMENT '商户状态 0-已注册1-未开通 2-已开通' after `super_agent` , 
	CHANGE `open_time` `open_time` datetime   NULL DEFAULT CURRENT_TIMESTAMP DEFAULT_GENERATED COMMENT '开通时间' after `status` , 
	CHANGE `stripe_card_fixed` `stripe_card_fixed` decimal(12,2)   NULL DEFAULT 30000.00 COMMENT '磁条卡封顶金额' after `credit_fixed` , 
	CHANGE `chip_card_fixed` `chip_card_fixed` decimal(12,2)   NULL DEFAULT 30000.00 COMMENT '芯片卡单笔限额' after `stripe_card_fixed` , 
	CHANGE `bank_name` `bank_name` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '开户行' after `bankno` , 
	CHANGE `biz_license` `biz_license` varchar(100)  COLLATE utf8_general_ci NULL COMMENT '营业执照编号' after `trans_ctrl` , 
	CHANGE `add_time` `add_time` timestamp   NULL DEFAULT CURRENT_TIMESTAMP DEFAULT_GENERATED on update CURRENT_TIMESTAMP COMMENT '录入时间' after `biz_license` , 
	CHANGE `trad_rate` `trad_rate` decimal(12,4)   NULL COMMENT '传统费率' after `biz_type` , 
	CHANGE `merch_level` `merch_level` int(11)   NULL COMMENT '商户级别(1 - A类,2 - B类, 4 - C类)' after `t0_rate` , 
	CHANGE `customerno` `customerno` varchar(30)  COLLATE utf8_general_ci NULL COMMENT '客户号' after `merch_level` , 
	CHANGE `silence_merch` `silence_merch` varchar(5)  COLLATE utf8_general_ci NULL DEFAULT 'N' COMMENT '沉默商户(Y 沉默商户,N不是)' after `customerno` , 
	CHANGE `t0_fixed` `t0_fixed` decimal(12,2)   NULL DEFAULT 0.00 COMMENT 't0封顶费率' after `silence_merch` , 
	CHANGE `merch_key` `merch_key` varchar(32)  COLLATE utf8_general_ci NULL COMMENT '交易密匙' after `jd_rate` , 
	CHANGE `online_url_notify` `online_url_notify` varchar(100)  COLLATE utf8_general_ci NULL COMMENT '交易通知地址' after `merch_key` , 
	CHANGE `online_url_return` `online_url_return` varchar(100)  COLLATE utf8_general_ci NULL COMMENT '交易跳转界面' after `online_url_notify` , 
	ADD COLUMN `gateway_rate_t1` decimal(12,4)   NULL DEFAULT 0.0000 COMMENT '网关T1费率' after `gateway_rate_t0` , 
	CHANGE `sale_level` `sale_level` int(11)   NULL DEFAULT 5 COMMENT '分销商级别(1-BOSS 2-高级 3-中级 4-低级 5-不属于分销商)' after `gateway_rate_t1` , 
	CHANGE `biz_license_img` `biz_license_img` varchar(520)  COLLATE utf8_general_ci NULL COMMENT '营业执照' after `zk_amount` , 
	CHANGE `identity3_img` `identity3_img` varchar(520)  COLLATE utf8_general_ci NULL COMMENT '身份证正面照' after `biz_license_img` , 
	CHANGE `identity1_img` `identity1_img` varchar(520)  COLLATE utf8_general_ci NULL COMMENT '身份证反面照' after `identity3_img` , 
	CHANGE `identity2_img` `identity2_img` varchar(520)  COLLATE utf8_general_ci NULL COMMENT '法人手持身份证照片' after `identity1_img` , 
	CHANGE `corporation_store_img` `corporation_store_img` varchar(520)  COLLATE utf8_general_ci NULL COMMENT '法人和线下门店合照' after `identity2_img` , 
	ADD COLUMN `company_store_img` varchar(520)  COLLATE utf8_general_ci NULL COMMENT '企业或店铺门头照片' after `corporation_store_img` , 
	CHANGE `cashier_img` `cashier_img` varchar(520)  COLLATE utf8_general_ci NULL COMMENT '前台或收银台相片' after `company_store_img` , 
	CHANGE `place_img` `place_img` varchar(520)  COLLATE utf8_general_ci NULL COMMENT '企业或店铺内部运营区域' after `cashier_img` , 
	CHANGE `card1_img` `card1_img` varchar(520)  COLLATE utf8_general_ci NULL COMMENT '银行卡正面照片' after `place_img` , 
	CHANGE `card2_img` `card2_img` varchar(520)  COLLATE utf8_general_ci NULL COMMENT '银行卡背面照片' after `card1_img` , 
	CHANGE `contract_img` `contract_img` varchar(520)  COLLATE utf8_general_ci NULL COMMENT '合同文件' after `card2_img` , 
	CHANGE `other_file` `other_file` varchar(520)  COLLATE utf8_general_ci NULL COMMENT '其它文件' after `contract_img` , 
	ADD COLUMN `remark` varchar(1000)  COLLATE utf8_general_ci NULL COMMENT '初审备注' after `other_file` , 
	ADD COLUMN `judg_remark` varchar(1000)  COLLATE utf8_general_ci NULL COMMENT '终审备注' after `remark` , 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' COMMENT '逻辑删除，1删除' after `judg_remark` , 
	ADD COLUMN `audit_time` datetime   NULL COMMENT '初审核时间' after `deleted` , 
	ADD COLUMN `judg_audit_time` datetime   NULL COMMENT '终审时间' after `audit_time` , 
	DROP COLUMN `head_img` , 
	DROP COLUMN `province` , 
	DROP COLUMN `area_code` , 
	DROP COLUMN `account_img` , 
	DROP COLUMN `credit1_img` , 
	DROP COLUMN `credit2_img` , 
	DROP COLUMN `city` , 
	DROP COLUMN `organ_no` , 
	DROP KEY `merch_index_1`, ADD KEY `merch_index_1`(`merchno`,`merch_name`) , 
	DROP KEY `PRIMARY`, ADD PRIMARY KEY(`id`,`merchno`) ;

/* Create table in target */
CREATE TABLE `merch_record`(
	`id` bigint(20) NOT NULL  auto_increment COMMENT '主键' , 
	`before_value` varchar(5000) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '操作前值' , 
	`after_value` varchar(5000) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '操作后值' , 
	`create_time` datetime NOT NULL  DEFAULT CURRENT_TIMESTAMP DEFAULT_GENERATED COMMENT '添加时间' , 
	`operator_id` int(15) NOT NULL  COMMENT '操作人id' , 
	`ip` varbinary(25) NULL  COMMENT '操作人当前ip' , 
	`success` bit(1) NULL  COMMENT '执行结果' , 
	`request_param` varchar(5000) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '请求参数' , 
	`request_func` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '请求方法名' , 
	`remark` varchar(500) COLLATE utf8mb4_0900_ai_ci NULL  DEFAULT '操作成功！' COMMENT '备注信息' , 
	PRIMARY KEY (`id`,`operator_id`) 
) ENGINE=InnoDB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_0900_ai_ci';


/* Alter table in target */
ALTER TABLE `merch_roler` 
	ADD COLUMN `deleted` bit(1)   NULL COMMENT '逻辑删除' after `branchno` ;

/* Alter table in target */
ALTER TABLE `merch_user` 
	ADD COLUMN `deleted` bit(1)   NULL COMMENT '逻辑删除' after `uuid` ;

/* Alter table in target */
ALTER TABLE `notify_merch_trans` 
	ADD COLUMN `send_count` int(11)   NULL DEFAULT 0 COMMENT '已发送次数' after `notify_data` , 
	CHANGE `notify_status` `notify_status` int(11)   NULL DEFAULT 0 COMMENT '通知状态(0-未通知 1-通知成功 2-通知失败)' after `send_count` , 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' after `biz_type` , 
	DROP COLUMN `send_time` ;

/* Alter table in target */
ALTER TABLE `notify_offical_trans` 
	ADD COLUMN `send_count` int(11)   NULL DEFAULT 0 COMMENT '已发送次数' after `amount` , 
	CHANGE `notify_status` `notify_status` int(11)   NULL DEFAULT 0 COMMENT '通知状态(0-未通知 1-通知成功 2-通知失败)' after `send_count` , 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' after `biz_type` , 
	DROP COLUMN `send_time` ;

/* Create table in target */
CREATE TABLE `oauth_access_token`(
	`token_id` varchar(256) COLLATE utf8_general_ci NULL  COMMENT '该字段的值是将access_token的值通过MD5加密后存储的.' , 
	`token` blob NULL  COMMENT '存储将OAuth2AccessToken.java对象序列化后的二进制数据, 是真实的AccessToken的数据值.' , 
	`authentication_id` varchar(128) COLLATE utf8_general_ci NOT NULL  COMMENT '该字段具有唯一性, 其值是根据当前的username(如果有),client_id与scope通过MD5加密生成的. 具体实现请参考DefaultAuthenticationKeyGenerator.java类.' , 
	`user_name` varchar(256) COLLATE utf8_general_ci NULL  COMMENT '登录时的用户名, 若客户端没有用户名(如grant_type=\"client_credentials\"),则该值等于client_id' , 
	`client_id` varchar(256) COLLATE utf8_general_ci NULL  , 
	`authentication` blob NULL  COMMENT '存储将OAuth2Authentication.java对象序列化后的二进制数据.' , 
	`refresh_token` varchar(256) COLLATE utf8_general_ci NULL  COMMENT '该字段的值是将refresh_token的值通过MD5加密后存储的.' , 
	`data` datetime NULL  DEFAULT CURRENT_TIMESTAMP DEFAULT_GENERATED , 
	PRIMARY KEY (`authentication_id`) 
) ENGINE=InnoDB DEFAULT CHARSET='utf8' COLLATE='utf8_general_ci';


/* Create table in target */
CREATE TABLE `oauth_approvals`(
	`userId` varchar(256) COLLATE utf8_general_ci NULL  , 
	`clientId` varchar(256) COLLATE utf8_general_ci NULL  , 
	`scope` varchar(256) COLLATE utf8_general_ci NULL  , 
	`status` varchar(10) COLLATE utf8_general_ci NULL  , 
	`expiresAt` timestamp NULL  , 
	`lastModifiedAt` timestamp NULL  
) ENGINE=InnoDB DEFAULT CHARSET='utf8' COLLATE='utf8_general_ci';


/* Create table in target */
CREATE TABLE `oauth_client_details`(
	`client_id` varchar(128) COLLATE utf8_general_ci NOT NULL  COMMENT '主键,必须唯一,不能为空.\r\n用于唯一标识每一个客户端(client); 在注册时必须填写(也可由服务端自动生成).\r\n对于不同的grant_type,该字段都是必须的. 在实际应用中的另一个名称叫appKey,与client_id是同一个概念.' , 
	`resource_ids` varchar(256) COLLATE utf8_general_ci NULL  COMMENT '客户端所能访问的资源id集合,多个资源时用逗号(,)分隔,如: \"unity-resource,mobile-resource\".' , 
	`client_secret` varchar(256) COLLATE utf8_general_ci NULL  COMMENT '用于指定客户端(client)的访问密匙; 在注册时必须填写(也可由服务端自动生成).\r\n对于不同的grant_type,该字段都是必须的. 在实际应用中的另一个名称叫appSecret,与client_secret是同一个概念.' , 
	`scope` varchar(256) COLLATE utf8_general_ci NULL  COMMENT '指定客户端申请的权限范围,可选值包括read,write,trust;若有多个权限范围用逗号(,)分隔,如: \"read,write\".' , 
	`authorized_grant_types` varchar(256) COLLATE utf8_general_ci NULL  COMMENT '指定客户端支持的grant_type,可选值包括authorization_code,password,refresh_token,implicit,client_credentials, 若支持多个grant_type用逗号(,)分隔,如: \"authorization_code,password\".' , 
	`web_server_redirect_uri` varchar(256) COLLATE utf8_general_ci NULL  COMMENT '客户端的重定向URI,可为空, 当grant_type为authorization_code或implicit时, 在Oauth的流程中会使用并检查与注册时填写的redirect_uri是否一致' , 
	`authorities` varchar(256) COLLATE utf8_general_ci NULL  COMMENT '指定客户端所拥有的Spring Security的权限值,可选, 若有多个权限值,用逗号(,)分隔, 如: \"ROLE_UNITY,ROLE_USER\".\r\n对于是否要设置该字段的值,要根据不同的grant_type来判断, 若客户端在Oauth流程中需要用户的用户名(username)与密码(password)的(authorization_code,password),\r\n则该字段可以不需要设置值,因为服务端将根据用户在服务端所拥有的权限来判断是否有权限访问对应的API.\r\n但如果客户端在Oauth流程中不需要用户信息的(implicit,client_credentials),\r\n则该字段必须要设置对应的权限值, 因为服务端将根据该字段值的权限来判断是否有权限访问对应的API.' , 
	`access_token_validity` int(11) NULL  COMMENT '设定客户端的access_token的有效时间值(单位:秒),可选, 若不设定值则使用默认的有效时间值(60 * 60 * 12, 12小时).\r\n在服务端获取的access_token JSON数据中的expires_in字段的值即为当前access_token的有效时间值.\r\n在项目中, 可具体参考DefaultTokenServices.java中属性accessTokenValiditySeconds.\r\n在实际应用中, 该值一般是由服务端处理的, 不需要客户端自定义.' , 
	`refresh_token_validity` int(11) NULL  COMMENT '设定客户端的refresh_token的有效时间值(单位:秒),可选, 若不设定值则使用默认的有效时间值(60 * 60 * 24 * 30, 30天).\r\n若客户端的grant_type不包括refresh_token,则不用关心该字段 在项目中, 可具体参考DefaultTokenServices.java中属性refreshTokenValiditySeconds.\r\n\r\n在实际应用中, 该值一般是由服务端处理的, 不需要客户端自定义.' , 
	`additional_information` varchar(4096) COLLATE utf8_general_ci NULL  COMMENT '这是一个预留的字段,在Oauth的流程中没有实际的使用,可选,但若设置值,必须是JSON格式的数据' , 
	`autoapprove` varchar(256) COLLATE utf8_general_ci NULL  COMMENT '设置用户是否自动Approval操作, 默认值为 \'false\', 可选值包括 \'true\',\'false\', \'read\',\'write\'.\r\n该字段只适用于grant_type=\"authorization_code\"的情况,当用户登录成功后,若该值为\'true\'或支持的scope值,则会跳过用户Approve的页面, 直接授权.\r\n该字段与 trusted 有类似的功能, 是 spring-security-oauth2 的 2.0 版本后添加的新属性.' , 
	PRIMARY KEY (`client_id`) 
) ENGINE=InnoDB DEFAULT CHARSET='utf8' COLLATE='utf8_general_ci';


/* Create table in target */
CREATE TABLE `oauth_client_token`(
	`token_id` varchar(256) COLLATE utf8_general_ci NULL  COMMENT '从服务器端获取到的access_token的值.' , 
	`token` blob NULL  COMMENT '这是一个二进制的字段, 存储的数据是OAuth2AccessToken.java对象序列化后的二进制数据.' , 
	`authentication_id` varchar(128) COLLATE utf8_general_ci NOT NULL  COMMENT '该字段具有唯一性, 是根据当前的username(如果有),client_id与scope通过MD5加密生成的.\r\n具体实现请参考DefaultClientKeyGenerator.java类.' , 
	`user_name` varchar(256) COLLATE utf8_general_ci NULL  COMMENT '登录时的用户名' , 
	`client_id` varchar(256) COLLATE utf8_general_ci NULL  , 
	PRIMARY KEY (`authentication_id`) 
) ENGINE=InnoDB DEFAULT CHARSET='utf8' COLLATE='utf8_general_ci';


/* Create table in target */
CREATE TABLE `oauth_code`(
	`code` varchar(256) COLLATE utf8_general_ci NULL  COMMENT '存储服务端系统生成的code的值(未加密).' , 
	`authentication` blob NULL  COMMENT '存储将AuthorizationRequestHolder.java对象序列化后的二进制数据.' 
) ENGINE=InnoDB DEFAULT CHARSET='utf8' COLLATE='utf8_general_ci';


/* Create table in target */
CREATE TABLE `oauth_refresh_token`(
	`token_id` varchar(256) COLLATE utf8_general_ci NULL  COMMENT '该字段的值是将refresh_token的值通过MD5加密后存储的.' , 
	`token` blob NULL  COMMENT '存储将OAuth2RefreshToken.java对象序列化后的二进制数据.' , 
	`authentication` blob NULL  COMMENT '存储将OAuth2Authentication.java对象序列化后的二进制数据.' 
) ENGINE=InnoDB DEFAULT CHARSET='utf8' COLLATE='utf8_general_ci';


/* Alter table in target */
ALTER TABLE `offical_info` 
	CHANGE `last_token_time` `last_token_time` datetime   NULL DEFAULT CURRENT_TIMESTAMP DEFAULT_GENERATED on update CURRENT_TIMESTAMP COMMENT '上次更新时间(yyyyMMddHH)' after `access_token` , 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' COMMENT '逻辑删除' after `last_token_time` ;

/* Alter table in target */
ALTER TABLE `online_channel` 
	CHANGE `channel_name` `channel_name` varchar(20)  COLLATE utf8_general_ci NULL COMMENT '渠道名' after `channel_code` , 
	CHANGE `status` `status` int(11)   NULL DEFAULT 1 COMMENT '渠道状态(1-已启用 2-已禁用)' after `card_t1_fee` , 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' COMMENT '逻辑删除' after `liquidation` ;

/* Alter table in target */
ALTER TABLE `online_channel_merch` 
	CHANGE `status` `status` int(11)   NULL DEFAULT 1 COMMENT '商户状态(1-已启用 2-已禁用)' after `key_md5` , 
	CHANGE `today_trans` `today_trans` decimal(12,2)   NULL DEFAULT 0.00 COMMENT '今日交易额' after `max_trans` , 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' COMMENT '逻辑删除' after `key_rsa` ;

/* Alter table in target */
ALTER TABLE `online_order` 
	CHANGE `merch_orderno` `merch_orderno` varchar(32)  COLLATE utf8_general_ci NULL COMMENT '商户订单号' after `merch_name` , 
	CHANGE `system_orderno` `system_orderno` varchar(32)  COLLATE utf8_general_ci NULL COMMENT '系统订单号' after `merch_orderno` , 
	CHANGE `status` `status` int(11)   NULL COMMENT '交易状态(0-下单成功 1-支付成功 2-支付失败)' after `channel_merchno` , 
	CHANGE `bank_name` `bank_name` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '银行名称' after `bank_code` , 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' after `merch_fee` ;

/* Create table in target */
CREATE TABLE `online_record`(
	`id` bigint(20) NOT NULL  auto_increment COMMENT '主键' , 
	`before_value` varchar(5000) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '操作前值' , 
	`after_value` varchar(5000) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '操作后值' , 
	`create_time` datetime NOT NULL  COMMENT '添加时间' , 
	`operator_id` int(15) NOT NULL  COMMENT '操作人id' , 
	`ip` varbinary(25) NULL  COMMENT '操作人当前ip' , 
	`success` bit(1) NULL  COMMENT '执行结果' , 
	`request_param` varchar(5000) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '请求参数' , 
	`request_func` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '请求方法名' , 
	PRIMARY KEY (`id`,`operator_id`) 
) ENGINE=InnoDB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_0900_ai_ci';


/* Alter table in target */
ALTER TABLE `online_route` 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' COMMENT '逻辑删除' after `settle_type` ;

/* Alter table in target */
ALTER TABLE `online_trans_history` 
	CHANGE `merch_orderno` `merch_orderno` varchar(32)  COLLATE utf8_general_ci NULL COMMENT '商户订单号' after `merch_name` , 
	CHANGE `system_orderno` `system_orderno` varchar(32)  COLLATE utf8_general_ci NULL COMMENT '系统订单号' after `merch_orderno` , 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' after `return_url` ;

/* Alter table in target */
ALTER TABLE `online_trans_today` 
	CHANGE `merch_orderno` `merch_orderno` varchar(32)  COLLATE utf8_general_ci NULL COMMENT '商户订单号' after `merch_name` , 
	CHANGE `system_orderno` `system_orderno` varchar(32)  COLLATE utf8_general_ci NULL COMMENT '系统订单号' after `merch_orderno` , 
	ADD COLUMN `merch_fee` decimal(12,2)   NULL COMMENT '商户手续费' after `amount` , 
	CHANGE `channel_code` `channel_code` varchar(30)  COLLATE utf8_general_ci NULL COMMENT '渠道编码' after `merch_fee` , 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' after `pay_type` , 
	DROP COLUMN `total_fee` ;

/* Alter table in target */
ALTER TABLE `pay_channel` 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' after `remark` ;

/* Alter table in target */
ALTER TABLE `pay_merch` 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' after `pay_type` ;

/* Alter table in target */
ALTER TABLE `pay_route` 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' after `status` ;

/* Alter table in target */
ALTER TABLE `qrcode_agent_fee` 
	CHANGE `agentno` `agentno` varchar(10)  COLLATE utf8_general_ci NOT NULL COMMENT '代理商号' after `branchno` , 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' COMMENT '逻辑删除,1删除' after `rate` , 
	ADD COLUMN `add_time` datetime   NULL DEFAULT CURRENT_TIMESTAMP DEFAULT_GENERATED COMMENT '最后操作时间' after `deleted` ;

/* Alter table in target */
ALTER TABLE `qrcode_branch_fee` 
	ADD COLUMN `deleted` int(2)   NULL DEFAULT 0 COMMENT '逻辑删除' after `rate` ;

/* Alter table in target */
ALTER TABLE `qrcode_channel_route` 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' COMMENT '逻辑删除' after `cur_index` ;

/* Alter table in target */
ALTER TABLE `qrcode_merch` 
	CHANGE `partner_id` `partner_id` varchar(32)  COLLATE utf8_general_ci NULL COMMENT '父节点' after `merchno` , 
	CHANGE `merch_name` `merch_name` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '商户名称' after `channel_code` , 
	CHANGE `app_id` `app_id` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '应用id' after `merch_name` , 
	CHANGE `app_key` `app_key` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '应用秘钥' after `app_id` , 
	CHANGE `key_rsa` `key_rsa` varchar(2000)  COLLATE utf8_general_ci NULL COMMENT 'rsa秘钥' after `df_type` , 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' COMMENT '逻辑删除' after `key_rsa` ;

/* Alter table in target */
ALTER TABLE `qrcode_merch_fee` 
	ADD COLUMN `add_time` datetime   NULL DEFAULT CURRENT_TIMESTAMP DEFAULT_GENERATED COMMENT '添加时间' after `rate` , 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' COMMENT '逻辑删除' after `add_time` ;

/* Alter table in target */
ALTER TABLE `qrcode_order` 
	CHANGE `branchno` `branchno` varchar(10)  COLLATE utf8_general_ci NULL COMMENT '机构号' after `id` , 
	CHANGE `branch_name` `branch_name` varchar(30)  COLLATE utf8_general_ci NULL COMMENT '机构名称' after `branchno` , 
	CHANGE `agentno` `agentno` varchar(10)  COLLATE utf8_general_ci NULL COMMENT '代理商号' after `branch_name` , 
	CHANGE `agent_name` `agent_name` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '代理商名称' after `agentno` , 
	CHANGE `merchno` `merchno` varchar(15)  COLLATE utf8_general_ci NULL COMMENT '商户号' after `agent_name` , 
	CHANGE `merch_name` `merch_name` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '商户名称' after `merchno` , 
	CHANGE `trans_date` `trans_date` varchar(10)  COLLATE utf8_general_ci NULL COMMENT '交易日期' after `merch_name` , 
	CHANGE `trans_time` `trans_time` varchar(8)  COLLATE utf8_general_ci NULL COMMENT '交易时间' after `trans_date` , 
	CHANGE `pay_type` `pay_type` int(11)   NULL COMMENT '支付类型' after `trans_time` , 
	CHANGE `scan_type` `scan_type` int(11)   NULL COMMENT '扫码方式' after `pay_type` , 
	CHANGE `traceno` `traceno` varchar(32)  COLLATE utf8_general_ci NULL COMMENT '商户流水号' after `scan_type` , 
	CHANGE `orderno` `orderno` varchar(32)  COLLATE utf8_general_ci NULL COMMENT '订单号' after `traceno` , 
	CHANGE `amount` `amount` decimal(12,2)   NULL COMMENT '交易金额' after `orderno` , 
	CHANGE `notify_url` `notify_url` varchar(500)  COLLATE utf8_general_ci NULL COMMENT '通知地址' after `amount` , 
	CHANGE `return_url` `return_url` varchar(300)  COLLATE utf8_general_ci NULL COMMENT '返回地址' after `notify_url` , 
	CHANGE `settle_type` `settle_type` int(11)   NULL COMMENT '结算方式(1-T+0结算 2-T+1结算' after `return_url` , 
	CHANGE `status` `status` int(11)   NULL COMMENT '订单状态(0-下单成功 1-支付成功 2-支付失败)' after `settle_type` , 
	CHANGE `status_desc` `status_desc` varchar(3000)  COLLATE utf8_general_ci NULL COMMENT '状态说明' after `status` , 
	CHANGE `channel_code` `channel_code` varchar(30)  COLLATE utf8_general_ci NULL COMMENT '渠道编码' after `status_desc` , 
	CHANGE `channel_merchno` `channel_merchno` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '渠道商户号' after `channel_code` , 
	CHANGE `channel_url` `channel_url` varchar(3000)  COLLATE utf8_general_ci NULL COMMENT '通道方交易地址' after `channel_merchno` , 
	CHANGE `merch_fee` `merch_fee` decimal(12,2)   NULL COMMENT '商户手续费' after `channel_url` , 
	CHANGE `channel_fee` `channel_fee` decimal(12,2)   NULL COMMENT '渠道手续费' after `merch_fee` , 
	CHANGE `branch_fee` `branch_fee` decimal(12,2)   NULL COMMENT '机构手续费' after `channel_fee` , 
	CHANGE `certno` `certno` varchar(20)  COLLATE utf8_general_ci NULL COMMENT '身份证号' after `branch_fee` , 
	CHANGE `mobile` `mobile` varchar(20)  COLLATE utf8_general_ci NULL COMMENT '手机号' after `certno` , 
	CHANGE `accountno` `accountno` varchar(20)  COLLATE utf8_general_ci NULL COMMENT '结算卡号' after `mobile` , 
	CHANGE `account_name` `account_name` varchar(30)  COLLATE utf8_general_ci NULL COMMENT '结算户名' after `accountno` , 
	CHANGE `bankno` `bankno` varchar(20)  COLLATE utf8_general_ci NULL COMMENT '联行号' after `account_name` , 
	CHANGE `bank_type` `bank_type` varchar(100)  COLLATE utf8_general_ci NULL COMMENT '银行类别' after `bankno` , 
	CHANGE `open_id` `open_id` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '用户的OpenId' after `bank_type` , 
	CHANGE `bank_name` `bank_name` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '支行名称' after `open_id` , 
	CHANGE `query_count` `query_count` int(11)   NULL DEFAULT 0 COMMENT '查询数量' after `bank_name` , 
	CHANGE `last_query_time` `last_query_time` varchar(19)  COLLATE utf8_general_ci NULL COMMENT '最后查询时间' after `query_count` , 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' after `channel_orderno` , 
	ADD COLUMN `merch_orderno` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '商户订单号' after `deleted` ;

/* Alter table in target */
ALTER TABLE `qrcode_order_history` 
	CHANGE `accountno` `accountno` varchar(25)  COLLATE utf8_general_ci NULL COMMENT '账户编号' after `mobile` , 
	CHANGE `bank_name` `bank_name` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '银行名称' after `open_id` , 
	ADD COLUMN `deleted` bit(1)   NULL COMMENT '逻辑删除' after `channel_orderno` , 
	ADD COLUMN `merch_orderno` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '商户订单号' after `deleted` ;

/* Create table in target */
CREATE TABLE `qrcode_record`(
	`id` bigint(20) NOT NULL  auto_increment COMMENT '主键' , 
	`before_value` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '操作前值' , 
	`after_value` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '操作后值' , 
	`create_time` datetime NOT NULL  COMMENT '添加时间' , 
	`operator_id` int(15) NOT NULL  COMMENT '操作人id' , 
	`ip` varbinary(25) NULL  COMMENT '操作人当前ip' , 
	`success` bit(1) NULL  COMMENT '执行结果' , 
	`request_param` varchar(2048) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '请求参数' , 
	`request_func` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '请求方法名' , 
	PRIMARY KEY (`id`,`operator_id`) 
) ENGINE=InnoDB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_0900_ai_ci';


/* Alter table in target */
ALTER TABLE `qrcode_route_merch` 
	ADD COLUMN `channel_merchno` varchar(32)  COLLATE utf8_general_ci NULL COMMENT '渠道商户号' after `channel_code` , 
	DROP COLUMN `partner_id` , 
	DROP KEY `qrcode_route_merch_partner_id`, ADD KEY `qrcode_route_merch_partner_id`(`channel_merchno`) ;

/* Alter table in target */
ALTER TABLE `qrcode_trans_error` 
	CHANGE `orderno` `orderno` varchar(30)  COLLATE utf8_general_ci NULL COMMENT '平台订单号' after `trans_time` ;

/* Alter table in target */
ALTER TABLE `qrcode_trans_history` 
	CHANGE `orderno` `orderno` varchar(32)  COLLATE utf8_general_ci NULL COMMENT '平台订单号' after `trans_time` , 
	ADD COLUMN `amount` decimal(12,2)   NULL COMMENT '交易金额' after `orderno` , 
	CHANGE `bar_code` `bar_code` varchar(500)  COLLATE utf8_general_ci NULL COMMENT '二维码信息' after `amount` , 
	ADD COLUMN `channel_merchno` varchar(32)  COLLATE utf8_general_ci NULL COMMENT '渠道商户编号' after `bar_code` , 
	CHANGE `termno` `termno` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '渠道终端号' after `channel_merchno` , 
	ADD COLUMN `merch_fee` decimal(12,2)   NULL DEFAULT 0.00 COMMENT '商户手续费' after `check_time` , 
	CHANGE `channel_fee` `channel_fee` decimal(12,2)   NULL DEFAULT 0.00 COMMENT '渠道手续费' after `merch_fee` , 
	CHANGE `branch_fee` `branch_fee` decimal(12,2)   NULL DEFAULT 0.00 COMMENT '机构手续费' after `channel_fee` , 
	CHANGE `traceno` `traceno` varchar(32)  COLLATE utf8_general_ci NULL COMMENT '流水号' after `agent_name` , 
	CHANGE `backup_orderno` `backup_orderno` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '渠道流水号' after `traceno` , 
	CHANGE `scan_type` `scan_type` int(11)   NULL COMMENT '扫码类型' after `channel_code` , 
	CHANGE `inter_type` `inter_type` int(11)   NULL COMMENT '接口类型' after `scan_type` , 
	CHANGE `mobile_seller` `mobile_seller` varchar(20)  COLLATE utf8_general_ci NULL COMMENT '卖家手机号' after `inter_type` , 
	CHANGE `mobile_buyer` `mobile_buyer` varchar(20)  COLLATE utf8_general_ci NULL COMMENT '买家手机号' after `mobile_seller` , 
	CHANGE `notify_url` `notify_url` varchar(300)  COLLATE utf8_general_ci NULL COMMENT '通知地址' after `mobile_buyer` , 
	CHANGE `return_url` `return_url` varchar(300)  COLLATE utf8_general_ci NULL COMMENT '返回地址' after `notify_url` , 
	CHANGE `pay_status` `pay_status` int(11)   NULL DEFAULT 0 COMMENT '付款状态(1-付款中 2-付款成功 3-付款失败)' after `t0_add_fee` , 
	CHANGE `pay_desc` `pay_desc` varchar(500)  COLLATE utf8_general_ci NULL COMMENT '支付说明' after `pay_status` , 
	CHANGE `mobile` `mobile` varchar(11)  COLLATE utf8_general_ci NULL COMMENT '手机号' after `pay_desc` , 
	CHANGE `bankno` `bankno` varchar(12)  COLLATE utf8_general_ci NULL COMMENT '联行号' after `mobile` , 
	CHANGE `bank_name` `bank_name` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '银行名称' after `bankno` , 
	CHANGE `bank_type` `bank_type` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '银行类别' after `bank_name` , 
	CHANGE `liquidator` `liquidator` int(11)   NULL DEFAULT 1 COMMENT '清算方(1-平台清算 2-虚拟账户)' after `bank_type` , 
	CHANGE `notify_status` `notify_status` int(11)   NULL DEFAULT 1 COMMENT '通知状态 1-无需通知 2-通知成功 3-通知失败' after `liquidator` , 
	ADD COLUMN `merch_orderno` varchar(30)  COLLATE utf8_general_ci NULL COMMENT '商户订单号' after `payment_status` , 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' after `merch_orderno` , 
	DROP COLUMN `dz_status` , 
	DROP COLUMN `trans_amount` , 
	DROP COLUMN `total_fee` , 
	DROP COLUMN `partner_id` , 
	DROP COLUMN `dz_date` , 
	DROP COLUMN `check_times` , 
	DROP KEY `qrcode_history_merchno` , 
	ADD KEY `qrcode_today_merchno`(`merchno`) , 
	DROP KEY `qrcode_trans_history_id_date_time` , 
	ADD KEY `qrcode_trans_today_date_time`(`trans_date`,`trans_time`) ;

/* Alter table in target */
ALTER TABLE `qrcode_trans_today` 
	ADD COLUMN `amount` decimal(12,2)   NULL COMMENT '交易金额' after `trans_time` , 
	CHANGE `bar_code` `bar_code` varchar(500)  COLLATE utf8_general_ci NULL COMMENT '二维码信息' after `amount` , 
	ADD COLUMN `channel_merchno` varchar(32)  COLLATE utf8_general_ci NULL COMMENT '渠道商户编号' after `bar_code` , 
	CHANGE `termno` `termno` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '渠道终端号' after `channel_merchno` , 
	ADD COLUMN `merch_fee` decimal(12,2)   NULL DEFAULT 0.00 COMMENT '商户手续费' after `check_time` , 
	CHANGE `channel_fee` `channel_fee` decimal(12,2)   NULL DEFAULT 0.00 COMMENT '渠道手续费' after `merch_fee` , 
	CHANGE `branch_fee` `branch_fee` decimal(12,2)   NULL DEFAULT 0.00 COMMENT '机构手续费' after `channel_fee` , 
	CHANGE `traceno` `traceno` varchar(32)  COLLATE utf8_general_ci NULL COMMENT '流水号' after `agent_name` , 
	CHANGE `backup_orderno` `backup_orderno` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '渠道流水号' after `traceno` , 
	CHANGE `scan_type` `scan_type` int(11)   NULL COMMENT '扫码类型' after `channel_code` , 
	CHANGE `inter_type` `inter_type` int(11)   NULL COMMENT '接口类型' after `scan_type` , 
	CHANGE `mobile_seller` `mobile_seller` varchar(20)  COLLATE utf8_general_ci NULL COMMENT '卖家手机号' after `inter_type` , 
	CHANGE `mobile_buyer` `mobile_buyer` varchar(20)  COLLATE utf8_general_ci NULL COMMENT '买家手机号' after `mobile_seller` , 
	CHANGE `notify_url` `notify_url` varchar(300)  COLLATE utf8_general_ci NULL COMMENT '通知地址' after `mobile_buyer` , 
	CHANGE `return_url` `return_url` varchar(300)  COLLATE utf8_general_ci NULL COMMENT '返回地址' after `notify_url` , 
	CHANGE `settle_type` `settle_type` int(11)   NULL DEFAULT 2 COMMENT '结算类型(1-T+0 2-T+1)' after `return_url` , 
	CHANGE `pay_status` `pay_status` int(11)   NULL DEFAULT 0 COMMENT '付款状态(0-待支付,1-支付成功 2-支付失败 3-支付未完成)' after `t0_add_fee` , 
	CHANGE `pay_desc` `pay_desc` varchar(500)  COLLATE utf8_general_ci NULL COMMENT '支付说明' after `pay_status` , 
	CHANGE `mobile` `mobile` varchar(11)  COLLATE utf8_general_ci NULL COMMENT '手机号' after `pay_desc` , 
	CHANGE `bankno` `bankno` varchar(12)  COLLATE utf8_general_ci NULL COMMENT '联行号' after `mobile` , 
	CHANGE `bank_name` `bank_name` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '银行名称' after `bankno` , 
	CHANGE `bank_type` `bank_type` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '银行类别' after `bank_name` , 
	ADD COLUMN `merch_orderno` varchar(30)  COLLATE utf8_general_ci NULL COMMENT '商户订单号' after `payment_status` , 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' after `merch_orderno` , 
	ADD COLUMN `batchno` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '批次号' after `deleted` , 
	ADD COLUMN `refno` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '参考号' after `batchno` , 
	CHANGE `orderno` `orderno` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '平台订单号' after `refno` , 
	ADD COLUMN `system_orderno` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '系统订单号' after `orderno` , 
	DROP COLUMN `trans_amount` , 
	DROP COLUMN `total_fee` , 
	DROP COLUMN `partner_id` , 
	DROP KEY `orderno` ;

/* Alter table in target */
ALTER TABLE `qrcode_trans_zk` 
	CHANGE `trans_date` `trans_date` timestamp   NULL COMMENT '下单日期' after `merch_name` , 
	CHANGE `orderno` `orderno` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '平台订单号' after `amount` , 
	CHANGE `add_time` `add_time` timestamp   NULL DEFAULT CURRENT_TIMESTAMP DEFAULT_GENERATED COMMENT '创建时间' after `channel_orderno` , 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' after `add_time` , 
	ADD COLUMN `merch_orderno` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '商户订单号' after `deleted` ;

/* Alter table in target */
ALTER TABLE `quick_bank_card` 
	CHANGE `add_time` `add_time` timestamp   NULL DEFAULT CURRENT_TIMESTAMP DEFAULT_GENERATED COMMENT '创建时间' after `status` , 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' after `sms_flag` ;

/* Alter table in target */
ALTER TABLE `quick_channel` 
	CHANGE `status` `status` int(11)   NULL DEFAULT 1 COMMENT '状态(1-启用 2-禁用)' after `agentno` , 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' COMMENT '逻辑删除' after `liquidation` ;

/* Alter table in target */
ALTER TABLE `quick_merch` 
	CHANGE `status` `status` int(11)   NULL DEFAULT 1 COMMENT '状态(1-启用 2-禁用)' after `key_rsa` , 
	CHANGE `settle_type` `settle_type` int(11)   NULL DEFAULT 2 COMMENT '1-T+0，2-T+1，4-全部' after `platform_merchno` , 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' COMMENT '逻辑删除' after `settle_type` ;

/* Alter table in target */
ALTER TABLE `quick_order` 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' after `last_query_time` ;

/* Create table in target */
CREATE TABLE `quick_record`(
	`id` bigint(20) NOT NULL  auto_increment COMMENT '主键' , 
	`before_value` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '操作前值' , 
	`after_value` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '操作后值' , 
	`create_time` datetime NOT NULL  COMMENT '添加时间' , 
	`operator_id` int(15) NOT NULL  COMMENT '操作人id' , 
	`ip` varbinary(25) NULL  COMMENT '操作人当前ip' , 
	`success` bit(1) NULL  COMMENT '执行结果' , 
	`request_param` varchar(2048) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '请求参数' , 
	`request_func` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '请求方法名' , 
	PRIMARY KEY (`id`,`operator_id`) 
) ENGINE=InnoDB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_0900_ai_ci';


/* Alter table in target */
ALTER TABLE `quick_route` 
	CHANGE `status` `status` int(11)   NULL DEFAULT 1 COMMENT '状态(1-启用 2-禁用)' after `time_end` , 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' COMMENT '逻辑删除' after `pay_mode` ;

/* Alter table in target */
ALTER TABLE `quick_route_merch` 
	ADD COLUMN `channel_merchno` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '渠道商户号' after `channel_code` , 
	DROP COLUMN `merchno` ;

/* Alter table in target */
ALTER TABLE `quick_trans_history` 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' after `payment_status` , 
	ADD COLUMN `system_orderno` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '系统订单号' after `deleted` , 
	ADD COLUMN `merch_orderno` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '商户订单号' after `system_orderno` ;

/* Alter table in target */
ALTER TABLE `quick_trans_today` 
	CHANGE `merchno` `merchno` varchar(15)  COLLATE utf8_general_ci NULL COMMENT '商户编号' after `agent_name` , 
	CHANGE `orderno` `orderno` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '平台订单号' after `traceno` , 
	ADD COLUMN `system_orderno` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '系统订单号' after `payment_status` , 
	ADD COLUMN `merch_orderno` varchar(50)  COLLATE utf8_general_ci NULL COMMENT '商户订单号' after `system_orderno` , 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' after `merch_orderno` ;

/* Create table in target */
CREATE TABLE `sys_menu`(
	`MENU_ID` int(11) NOT NULL  auto_increment COMMENT '菜单ID' , 
	`MENU_NAME` varchar(255) COLLATE utf8_general_ci NULL  COMMENT '菜单名称' , 
	`MENU_URL` varchar(255) COLLATE utf8_general_ci NULL  COMMENT '菜单地址' , 
	`PARENT_ID` varchar(100) COLLATE utf8_general_ci NULL  COMMENT '父级ID' , 
	`MENU_ORDER` varchar(100) COLLATE utf8_general_ci NULL  COMMENT '菜单顺序' , 
	`MENU_ICON` varchar(30) COLLATE utf8_general_ci NULL  COMMENT '菜单图标' , 
	`MENU_TYPE` varchar(10) COLLATE utf8_general_ci NULL  COMMENT '菜单类型' , 
	PRIMARY KEY (`MENU_ID`) 
) ENGINE=InnoDB DEFAULT CHARSET='utf8' COLLATE='utf8_general_ci' COMMENT='*系统菜单表';


/* Alter table in target */
ALTER TABLE `system_area` 
	ADD COLUMN `deleted` bit(1)   NULL COMMENT '逻辑删除' after `province` ;

/* Alter table in target */
ALTER TABLE `system_card_bin` 
	ADD COLUMN `deleted` bit(1)   NULL COMMENT '逻辑删除' after `card_length` ;

/* Alter table in target */
ALTER TABLE `system_logs` 
	CHANGE `opt_remark` `opt_remark` varchar(10240)  COLLATE utf8_general_ci NULL COMMENT '内容详情' after `opt_type` ;

/* Alter table in target */
ALTER TABLE `system_mcc` 
	ADD COLUMN `deleted` bit(1)   NULL COMMENT '逻辑删除' after `mcc_memo` ;

/* Alter table in target */
ALTER TABLE `system_param` 
	ADD COLUMN `deleted` bit(1)   NULL COMMENT '逻辑删除' after `param_memo` ;

/* Create table in target */
CREATE TABLE `system_record`(
	`id` bigint(20) NOT NULL  auto_increment COMMENT '主键' , 
	`before_value` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '操作前值' , 
	`after_value` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '操作后值' , 
	`create_time` datetime NOT NULL  COMMENT '添加时间' , 
	`operator_id` int(15) NOT NULL  COMMENT '操作人id' , 
	`ip` varbinary(25) NULL  COMMENT '操作人当前ip' , 
	`success` bit(1) NULL  COMMENT '执行结果' , 
	`request_param` varchar(2048) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '请求参数' , 
	`request_func` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '请求方法名' , 
	PRIMARY KEY (`id`,`operator_id`) 
) ENGINE=InnoDB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_0900_ai_ci';


/* Create table in target */
CREATE TABLE `term_record`(
	`id` bigint(20) NOT NULL  auto_increment COMMENT '主键' , 
	`before_value` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '操作前值' , 
	`after_value` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '操作后值' , 
	`create_time` datetime NOT NULL  COMMENT '添加时间' , 
	`operator_id` int(15) NOT NULL  COMMENT '操作人id' , 
	`ip` varbinary(25) NULL  COMMENT '操作人当前ip' , 
	`success` bit(1) NULL  COMMENT '执行结果' , 
	`request_param` varchar(2048) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '请求参数' , 
	`request_func` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '请求方法名' , 
	PRIMARY KEY (`id`,`operator_id`) 
) ENGINE=InnoDB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_0900_ai_ci';


/* Alter table in target */
ALTER TABLE `trans_backup` 
	CHANGE `traceno` `traceno` varchar(32)  COLLATE utf8_general_ci NULL COMMENT '流水号' after `batchno` , 
	CHANGE `refno` `refno` varchar(32)  COLLATE utf8_general_ci NULL COMMENT '参考号' after `traceno` , 
	ADD COLUMN `merch_fee` decimal(12,2)   NULL COMMENT '商户手续费' after `trans_model` , 
	CHANGE `channel_code` `channel_code` varchar(10)  COLLATE utf8_general_ci NULL COMMENT '渠道编码' after `merch_fee` , 
	CHANGE `branchno` `branchno` varchar(20)  COLLATE utf8_general_ci NULL COMMENT '机构编号' after `liquid_traceno` , 
	CHANGE `branch_name` `branch_name` varchar(30)  COLLATE utf8_general_ci NULL COMMENT '机构名称' after `branchno` , 
	CHANGE `business_type` `business_type` varchar(5)  COLLATE utf8_general_ci NULL COMMENT '业务类型' after `branch_name` , 
	CHANGE `pay_type` `pay_type` int(11)   NULL COMMENT '支付方式' after `business_type` , 
	CHANGE `first_agent_name` `first_agent_name` varchar(15)  COLLATE utf8_general_ci NULL COMMENT '一级代理商名称' after `pay_type` , 
	CHANGE `ip` `ip` varchar(15)  COLLATE utf8_general_ci NULL COMMENT 'ip地址' after `first_agent_name` , 
	CHANGE `pay_fee` `pay_fee` decimal(12,2)   NULL COMMENT '提现手续费' after `ip` , 
	CHANGE `first_agentno` `first_agentno` varchar(10)  COLLATE utf8_general_ci NULL COMMENT '一级代理商编号' after `pay_fee` , 
	CHANGE `agentno` `agentno` varchar(10)  COLLATE utf8_general_ci NULL COMMENT '代理商编号' after `first_agentno` , 
	CHANGE `agent_name` `agent_name` varchar(30)  COLLATE utf8_general_ci NULL COMMENT '代理商名称' after `agentno` , 
	CHANGE `liquid_status` `liquid_status` int(11)   NULL DEFAULT 1 COMMENT '结算状态1.未出款，2.出款中，3.已出款' after `agent_name` , 
	CHANGE `identity_hold_img` `identity_hold_img` varchar(100)  COLLATE utf8_general_ci NULL COMMENT '身份证正面照' after `head_fee` , 
	CHANGE `card_hold_img` `card_hold_img` varchar(100)  COLLATE utf8_general_ci NULL COMMENT '手持银行卡' after `identity_hold_img` , 
	CHANGE `audit_reason` `audit_reason` varchar(200)  COLLATE utf8_general_ci NULL COMMENT '审核理由' after `t0_head_fee` , 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' after `liquid_desc` , 
	DROP COLUMN `total_fee` ;

/* Alter table in target */
ALTER TABLE `trans_real` 
	CHANGE `traceno` `traceno` varchar(32)  COLLATE utf8_general_ci NULL COMMENT '流水号' after `batchno` , 
	CHANGE `refno` `refno` varchar(32)  COLLATE utf8_general_ci NULL COMMENT '参考号' after `traceno` , 
	CHANGE `ip` `ip` varchar(15)  COLLATE utf8_general_ci NULL COMMENT 'IP' after `card_organ` , 
	CHANGE `address` `address` varchar(200)  COLLATE utf8_general_ci NULL COMMENT '物理地址' after `ip` , 
	CHANGE `merch_name` `merch_name` varchar(30)  COLLATE utf8_general_ci NULL COMMENT '商户名称' after `address` , 
	CHANGE `branchno` `branchno` varchar(15)  COLLATE utf8_general_ci NULL COMMENT '机构编号' after `merch_name` , 
	CHANGE `branch_name` `branch_name` varchar(15)  COLLATE utf8_general_ci NULL COMMENT '机构名称' after `branchno` , 
	CHANGE `agentno` `agentno` varchar(10)  COLLATE utf8_general_ci NULL COMMENT '代理商编号' after `branch_name` , 
	CHANGE `agent_name` `agent_name` varchar(15)  COLLATE utf8_general_ci NULL COMMENT '代理商名称' after `agentno` , 
	CHANGE `first_agentno` `first_agentno` varchar(10)  COLLATE utf8_general_ci NULL COMMENT '所属一级代理商编号' after `agent_name` , 
	CHANGE `first_agent_name` `first_agent_name` varchar(15)  COLLATE utf8_general_ci NULL COMMENT '一级代理商名称' after `first_agentno` , 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' after `first_agent_name` ;

/* Create table in target */
CREATE TABLE `trans_record`(
	`id` bigint(20) NOT NULL  auto_increment COMMENT '主键' , 
	`before_value` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '操作前值' , 
	`after_value` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '操作后值' , 
	`create_time` datetime NOT NULL  COMMENT '添加时间' , 
	`operator_id` int(15) NOT NULL  COMMENT '操作人id' , 
	`ip` varbinary(25) NULL  COMMENT '操作人当前ip' , 
	`success` bit(1) NULL  COMMENT '执行结果' , 
	`request_param` varchar(2048) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '请求参数' , 
	`request_func` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '请求方法名' , 
	PRIMARY KEY (`id`,`operator_id`) 
) ENGINE=InnoDB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_0900_ai_ci';


/* Alter table in target */
ALTER TABLE `trans_today` 
	CHANGE `merch_name` `merch_name` varchar(30)  COLLATE utf8_general_ci NULL COMMENT '商户名' after `merchno` , 
	CHANGE `traceno` `traceno` varchar(32)  COLLATE utf8_general_ci NULL COMMENT '商户流水号' after `batchno` , 
	ADD COLUMN `merch_fee` decimal(12,2)   NULL COMMENT '商户手续费' after `trans_model` , 
	CHANGE `channel_code` `channel_code` varchar(10)  COLLATE utf8_general_ci NULL COMMENT '渠道编码' after `merch_fee` , 
	ADD COLUMN `deleted` bit(1)   NULL DEFAULT b'0' after `liquidator` , 
	DROP COLUMN `total_fee` ;

/* Alter table in target */
ALTER TABLE `virtual_card` 
	ADD COLUMN `password` varchar(32)  COLLATE utf8_general_ci NULL COMMENT '账户密码' after `rate_code` , 
	CHANGE `avail_amount` `avail_amount` decimal(12,2)   NULL DEFAULT 0.00 COMMENT '可用资金' after `password` , 
	DROP COLUMN `passwd` ;

/* Create table in target */
CREATE TABLE `virtual_record`(
	`id` bigint(20) NOT NULL  auto_increment COMMENT '主键' , 
	`before_value` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '操作前值' , 
	`after_value` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '操作后值' , 
	`create_time` datetime NOT NULL  COMMENT '添加时间' , 
	`operator_id` int(15) NOT NULL  COMMENT '操作人id' , 
	`ip` varbinary(25) NULL  COMMENT '操作人当前ip' , 
	`success` bit(1) NULL  COMMENT '执行结果' , 
	`request_param` varchar(2048) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '请求参数' , 
	`request_func` varchar(255) COLLATE utf8mb4_0900_ai_ci NULL  COMMENT '请求方法名' , 
	PRIMARY KEY (`id`,`operator_id`) 
) ENGINE=InnoDB DEFAULT CHARSET='utf8mb4' COLLATE='utf8mb4_0900_ai_ci';

/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;